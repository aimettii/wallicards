<?php

use App\Models\Role;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('auth/login', 'AuthAPIController@loginByToken');

Route::post('wallets/{uuid}/register', 'Client\ClientAuthAPIController@register');
Route::get('wallets/{uuid}/template-design', 'Client\ClientAuthAPIController@templateDesign');
Route::post('wallets/{uuid}/send-sms/{phone}', 'Client\ClientAuthAPIController@sendSMS');
Route::post('wallets/{uuid}/verify-device/{phone}/{code}', 'Client\ClientAuthAPIController@verifyDevice');

Route::get('clients/pkpass/{hash}', 'Client\ClientAuthAPIController@getPkpassFile');

Route::middleware('auth:sanctum')->group(function () {
    Route::group(['middleware' => [
        Role::middlewareOnly(Role::ADMIN)]
    ], function () {
        Route::resource('partners', 'PartnerAPIController');
        Route::get('/partners/{id}/employees', 'PartnerAPIController@employees');

//        Route::resource('employees', 'EmployeeAPIController');
    });

    Route::group(['middleware' => [
        Role::middlewareAnyRoles([Role::ADMIN, Role::PARTNER])
    ]
    ], function () {

    });

    Route::group(['middleware' => [
        Role::middlewareAnyRoles([Role::PARTNER, Role::EMPLOYEE])]
    ], function () {
        Route::resource('employees', 'Partner\PartnerEmployeeAPIController');
        Route::resource('wallets', 'Company\CompanyWalletsAPIController');
        Route::resource('wallet_certificates', 'Company\CompanyWalletCertificatesAPIController');
        Route::resource('clients', 'Company\CompanyClientsAPIController');
        Route::resource('pushes', 'Company\CompanyPushesAPIController');
        Route::get('pushes/create/wallets', 'Company\CompanyPushesAPIController@wallets');
        Route::get('pushes/create/clients', 'Company\CompanyPushesAPIController@clients');
        Route::get('clients/{id}/transactions', 'Company\CompanyClientsAPIController@transactions');
        Route::post('clients/{id}/transactions', 'Company\CompanyClientsAPIController@transactionsStore');
        Route::get('clients/{id}/points-history', 'Company\CompanyClientsAPIController@pointsHistory');
        Route::resource('auditories', 'AuditoryAPIController');
        Route::resource('journal', 'JournalAPIController');
    });


    Route::get('account', 'AccountAPIController');

    Route::resource('users', 'UsersAPIController');

//    Route::resource('companies', 'CompanyAPIController');

//    Route::resource('wallets', 'WalletAPIController');
});

Route::resource('transactions', 'TransactionAPIController');



Route::resource('journals', 'JournalAPIController');