<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use InfyOm\Generator\Utils\ResponseUtil;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

function sendResponse($result, $message)
{
    return Response::json(ResponseUtil::makeResponse($message, $result));
}

function sendError($error, $code = 404)
{
    return Response::json(ResponseUtil::makeError($error), $code);
}

function sendSuccess($message)
{
    return Response::json([
        'success' => true,
        'message' => $message
    ], 200);
}

Route::get('/', function () {
    return view('welcome');
});

//Route::post('auth/login', function(Request $request) {
//    $email = $request->get('email');
//    $password = $request->get('password');
//
////        $baseFieldKey = filter_var($email, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';
//    $baseFieldKey = 'email';
//
//    if (\Auth::attempt([
//        $baseFieldKey => $email,
//        'password' => $password
//    ])) {
//        return sendSuccess('Authorization successful, Cookies have been set');
//    }else{
//        return sendError('The provided credentials are incorrect', 401);
//    }
//});
Route::post('auth/login', 'API\AuthAPIController@loginByCookie');

Route::post('auth/logout', 'API\AuthAPIController@logout');

//Route::middleware('auth:sanctum')->group(function () {
//    Route::get('storage/wallet/{hash}', 'API\AuthAPIController@logout');
//});

Route::get('storage/wallet/{uuid}/background-image', 'API\WalletsAPIController@backgroundImage');
Route::get('storage/wallet/{uuid}/logo-image', 'API\WalletsAPIController@logoImage');
Route::get('storage/wallet/{uuid}/push-notification-icon-image', 'API\WalletsAPIController@pushNotificationIconImage');

//Route::post('auth/logout', function(Request $request) {
//    \Auth::logout();
//
//    return sendSuccess('Cleaning the authentication information was successful');
//});

Route::middleware('auth:sanctum')->group(function () {
    Route::get('account', 'API\AccountAPIController');
});


Route::prefix('v1')->group(function () {
    Route::middleware('verify_apple_client')->group(function () {
        Route::post('devices/{deviceId}/registrations/{passType}/{serial}', 'API\Client\ClientAuthAPIController@registerDeviceWebhook');
        Route::get('passes/{passType}/{serial}', 'API\Client\ClientAuthAPIController@getLatestVersionOfPassWebhook');
        Route::delete('devices/{deviceId}/registrations/{passType}/{serial}', 'API\Client\ClientAuthAPIController@deleteDeviceWebhook');
    });
    Route::post('log', 'API\Client\ClientAuthAPIController@logWebhook');
    Route::get('devices/{deviceId}/registrations/{passType}', 'API\Client\ClientAuthAPIController@getSerialNumbersWebhook');
});

