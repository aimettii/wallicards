<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->dropColumn('status_on_device', 'platform');
            $table->string('device_id')->nullable()->change();
            $table->string('push_token')->nullable()->change();
        });

        Schema::table('clients', function (Blueprint $table) {
            $table->enum('status_on_device', \App\Models\Client::defaultDeviceStatuses())->after('push_token');
            $table->enum('platform', \App\Models\Client::defaultMobilePlatforms())->nullable()->after('push_token');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clients', function (Blueprint $table) {
            //
        });
    }
}
