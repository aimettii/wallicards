<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePushTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pushes', function (Blueprint $table) {
            $table->id();
            $table->morphs('handleable');
            $table->longText('message');
            $table->dateTime('will_be_executed_at');
            $table->timestamps();
        });

        Schema::create('push_client', function (Blueprint $table) {
            $table->id();
            $table->enum('status', ['performed', 'failed', 'process'])->default('process');
            $table->foreignId('push_id')->constrained('pushes');
            $table->foreignId('client_id')->constrained('clients');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('push_client');
        Schema::dropIfExists('pushes');
        Schema::enableForeignKeyConstraints();
    }
}
