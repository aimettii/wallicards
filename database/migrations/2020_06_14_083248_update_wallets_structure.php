<?php

use App\Models\Wallet;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateWalletsStructure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wallet_property_values', function (Blueprint $table) {
            if (Schema::hasColumn('wallet_property_values', 'wallet_type_id')) {
                $table->dropForeign(['wallet_type_id']);
                $table->dropColumn('wallet_type_id');
            }

            if (! Schema::hasColumn('wallet_property_values', 'wallet_id')){
                $table->foreignId('wallet_id')->constrained('wallets');
            }
        });

        Schema::disableForeignKeyConstraints();

        \DB::table('wallet_types')->truncate();

        \DB::table('wallet_types')->insertOrIgnore([
            ['name' => Wallet::TYPE_DISCOUNT],
            ['name' => Wallet::TYPE_BONUS],
            ['name' => Wallet::TYPE_COUPON],
            ['name' => Wallet::TYPE_CHOP],
        ]);

        $propertyTypes = collect(\App\Models\WalletPropertyType::getAllTypes())->map(function ($type) {
            return ['name' => $type];
        })->all();

        \DB::table('wallet_property_types')->truncate();

        \DB::table('wallet_property_types')->insertOrIgnore($propertyTypes);
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wallet_property_values', function (Blueprint $table) {
            if (Schema::hasColumn('wallet_property_values', 'wallet_id')) {
                $table->dropForeign(['wallet_id']);
                $table->dropColumn('wallet_id');
            }
        });
    }
}
