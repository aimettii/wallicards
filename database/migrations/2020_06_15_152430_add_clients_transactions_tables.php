<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddClientsTransactionsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('serial_number')->unique();
            $table->foreignId('wallet_id')->constrained('wallets');
            $table->string('name');
            $table->enum('gender', [\App\Models\Client::GENDER_MAN, \App\Models\Client::GENDER_WOMAN]);
            $table->date('date_of_birth');
            $table->integer('balance');
            $table->string('device_id');
            $table->string('push_token');
            $table->enum('platform', \App\Models\Client::defaultMobilePlatforms());
            $table->enum('status_on_device', \App\Models\Client::defaultDeviceStatuses());
            $table->timestamps();
        });

        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->morphs('handleable');
            $table->foreignId('client_id')->constrained('clients');
            $table->enum('type', \App\Models\Transaction::defaultTypes());
            $table->timestamps();
        });

        Schema::create('transaction_property_types', function (Blueprint $table) {
            $table->id();
            $table->string('name');
        });

        Schema::create('transaction_options', function (Blueprint $table) {
            $table->id();
            $table->foreignId('transaction_id')->constrained('transactions');
            $table->foreignId('transaction_property_type_id')->constrained('transaction_property_types');
            $table->json('transaction_property_value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('clients');
        Schema::dropIfExists('transactions');
        Schema::dropIfExists('transaction_property_types');
        Schema::dropIfExists('transaction_options');
        Schema::enableForeignKeyConstraints();
    }
}
