<?php

use App\Models\Company;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBaseDevelopmentTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('entities');

        Schema::create('phones', function (Blueprint $table) {
            $table->id();
            $table->string('phone_number');
            $table->morphs('phoneable');
        });

        Schema::create('companies', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('address');
            $table->text('note')->nullable();
            $table->enum('status', Company::getAllStatuses())->default(Company::STATUS_ACTIVE);
            $table->timestamps();
        });

        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->unique()->constrained('users');
            $table->foreignId('company_id')->constrained('companies');
        });

        Schema::create('wallet_types', function (Blueprint $table) {
            $table->id();
            $table->string('name');
        });

        Schema::create('wallets', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->foreignId('company_id')->constrained('companies');
            $table->foreignId('wallet_type_id')->constrained('wallet_types');
        });

        Schema::create('wallet_property_types', function (Blueprint $table) {
            $table->id();
            $table->string('name');
        });

        Schema::create('wallet_property_values', function (Blueprint $table) {
            $table->id();
            $table->foreignId('wallet_type_id')->constrained('wallet_types');
            $table->foreignId('wallet_property_type_id')->constrained('wallet_property_types');
            $table->json('wallet_property_value');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('phoneables');
        Schema::dropIfExists('phones');
        Schema::dropIfExists('employees');
        Schema::dropIfExists('companies');
        Schema::dropIfExists('wallet_property_values');
        Schema::dropIfExists('wallets');
        Schema::dropIfExists('wallet_types');
        Schema::dropIfExists('wallet_property_types');
        Schema::enableForeignKeyConstraints();
    }
}
