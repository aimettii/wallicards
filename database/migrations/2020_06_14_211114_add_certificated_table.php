<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCertificatedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wallet_certificates', function (Blueprint $table) {
            $table->id();
            $table->string('p12_file');
            $table->string('wwdr_file');
            $table->string('p12_password');
            $table->string('team_identifier');
            $table->string('organization_name');
            $table->string('pass_type_identifier');
            $table->foreignId('company_id')->constrained('companies');
            $table->timestamps();
        });

        Schema::table('wallets', function (Blueprint $table) {
            $table->uuid('uuid');
            $table->foreignId('certificate_id')->constrained('wallet_certificates');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('wallet_certificates');
        Schema::enableForeignKeyConstraints();
    }
}
