<?php

use Illuminate\Database\Seeder;

class WalletPropertyTypesSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = collect(\App\Models\WalletPropertyType::getAllTypes())->map(function ($type) {
            return ['name' => $type];
        })->all();

        foreach ($types as $type) {
            if(! \App\Models\WalletPropertyType::where('name', $type['name'])->first()) {
                \DB::table('wallet_property_types')->insert($type);
            }
        }

        $oldTypes = \App\Models\WalletPropertyType::whereNotIn('name', \App\Models\WalletPropertyType::getAllTypes());

        \App\Models\WalletPropertyValues::whereIn('wallet_property_type_id', $oldTypes->get()->pluck('id')->all())->delete();

        $oldTypes->delete();
    }
}
