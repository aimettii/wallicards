<?php

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use App\Models\Permission;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Ask for db migration refresh, default is no
        if ($this->command->confirm('Do you wish to refresh migration before seeding, it will clear all old data ?')) {
            // Call the php artisan migrate:refresh
            $this->command->call('migrate:refresh');
            $this->command->warn("Data cleared, starting from blank database.");
        }

        // Seed the default permissions
        $permissions = Permission::defaultPermissions();

        foreach ($permissions as $perms) {
            Permission::firstOrCreate(['name' => $perms, 'guard_name' => 'sanctum']);
        }

        $this->command->info('Default Permissions added.');

        // Seed the default permissions
        $employeePermissions = Permission::employeePermissions();

        foreach ($employeePermissions as $employeePermission) {
            Permission::firstOrCreate(['name' => $employeePermission, 'guard_name' => 'sanctum']);
        }

        $this->command->info('Employee Permissions added.');

        $separatedRolesString = implode(",", Role::defaultRoles());

        if ($this->command->confirm('Create Roles for user, default is ' . $separatedRolesString . '? [y|N]', true)) {

            // создать роли
            foreach(Role::defaultRoles() as $role) {
                $role = Role::firstOrCreate(['name' => trim($role), 'guard_name' => 'sanctum']);

                if( $role->name == Role::ADMIN ) {
                    // дать все права
                    $role->syncPermissions(Permission::all());
                    $this->command->info('Admin granted all the permissions');
                } else if ($role->name == Role::PARTNER) {
                    $role->syncPermissions(Permission::whereIn('name', Permission::employeePermissions())->get());
                } else {
                    $role->syncPermissions([]);
                }

                // Создать одного пользователя на каждую роль
                $this->createUser($role);
            }

            $this->command->info('Roles ' . $separatedRolesString . ' added successfully');

        }

        $this->command->warn('All done :)');
    }

    /**
     * Create a user with given role
     *
     * @param $role
     */
    private function createUser($role)
    {
        $user = factory(User::class)->create();
        $user->assignRole($role);

        if( $role->name == Role::ADMIN ) {
            $this->command->info('Here is your admin details to login:');
            $this->command->warn($user->email);
            $this->command->warn('Password is "password"');
        }
    }
}
