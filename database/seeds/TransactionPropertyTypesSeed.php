<?php

use Illuminate\Database\Seeder;

class TransactionPropertyTypesSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        $types = collect(\App\Models\TransactionOptionType::defaultTypes())->map(function ($type) {
            return ['name' => $type];
        })->all();

        foreach ($types as $type) {
            $query = \App\Models\TransactionOptionType::where('name', $type['name']);
            $count = $query->count();
            if(! $query->first()) {
                \DB::table('transaction_property_types')->insert($types);
            }
            if($count > 1) {
                $ids = \App\Models\TransactionOptionType::where('name', $type['name'])->pluck('id')->all();
                unset($ids[0]);

                if (\App\Models\TransactionOption::whereIn('transaction_property_type_id', $ids)->count() > 0) {
                    throw new \Exception('Founded options');
                };

                \App\Models\TransactionOptionType::whereIn('id', $ids)->delete();
            }
        }

        $oldTypes = \App\Models\TransactionOptionType::whereNotIn('name', \App\Models\TransactionOptionType::defaultTypes());

        \App\Models\TransactionOption::whereIn('transaction_property_type_id', $oldTypes->get()->pluck('id')->all())->delete();

        $oldTypes->delete();
    }
}
