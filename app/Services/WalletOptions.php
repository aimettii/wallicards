<?php

namespace App\Services;

use App\Models\Wallet;
use App\Models\WalletPropertyType;
use App\Models\WalletPropertyValues;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class WalletOptions
{
    public $wallet;

    public function setWalletInstance(Wallet $wallet)
    {
        $this->wallet = $wallet;

        return $this;
    }

    public function option($optionName)
    {
        $optionModel = $optionName instanceof WalletPropertyValues ? $optionName : $this->getOptionModel($optionName);

        if ($optionModel) {
            $manyOptions = array_key_exists(0, $optionModel->value);
            $arrOptions = !$manyOptions ? [$optionModel->value] : $optionModel->value;

            $collectionOptionInstances = collect();

            foreach ($arrOptions as $arrOption) {
                $collectionOptionInstances->push(new class ($arrOption, $optionModel)
                {
                    public $value;
                    public $type;
                    public $fields;
                    public $optionName;
                    public $manyFields = false;

                    public function __construct($option, $optionModel)
                    {
                        if (isset($option['fields'])) {
                            $this->manyFields = true;
                            $this->fields = (object)$option['fields'];
                            $this->value = (object)$option['value'];
                        } else {
                            $this->value = $option['value'];
                            $this->type = $option['type'];
                        }
                        $this->optionName = $optionModel->type;
                    }

                    public function generateShortCodes($shortCodes)
                    {
                        $shortCodes = collect($shortCodes)->mapWithKeys(function ($value, $key) {
                            return ['${' . $key . '}' => $value];
                        })->all();
                       
                        if ($this->manyFields) {
                            foreach ($this->fields as $fieldName => $fieldType) {
                                if ($fieldType == 'string') {
                                    $this->value->$fieldName = strtr($this->value->$fieldName, $shortCodes);
                                }
                            }
                        } else {
                            if ($this->type == 'string') {
                                $this->value = strtr($this->value, $shortCodes);
                            } else {
                                throw new BadRequestException('Нельзя использовать генерацию шорт-кодов не на текстовом поле'); // @todo BadRequestException
                            }
                        }

                        return $this;
                    }
                });
            }

            if ($manyOptions) {
                return $collectionOptionInstances;
            } else {
                return $collectionOptionInstances->first();
            }
        } else {
            return null;
        }
    }

    public function options()
    {
        if (!$this->wallet) {
            throw new BadRequestException('Не указан wallet шаблон для работы с опциями'); // @todo BadRequestException
        }

        return $this->wallet->options->map(function ($option) {
            return $this->option($option);
        });
    }

    public function optionInstanceToRequestParams()
    {
        return $this->wallet->options->mapWithKeys(function ($option) {
            $optionInstance = $this->option($option);

            if ($optionInstance instanceof Collection) {
                $optionName = null;

                $items = [];

                foreach ($optionInstance as $item) {
                    $optionName = $item->optionName;
                    $items[] = $item->value;
                }

                return [$optionName => $items];
            } else {
                $value = $optionInstance->value;

                if (in_array($optionInstance->optionName,
                    [WalletPropertyType::LOGO_IMAGE, WalletPropertyType::BACKGROUND_IMAGE, WalletPropertyType::PUSH_NOTIFICATION_ICON])
                ) {
                    if ($optionInstance->optionName === WalletPropertyType::BACKGROUND_IMAGE) {
                        $value = $this->wallet->background_image_url;
                    }

                    if ($optionInstance->optionName === WalletPropertyType::LOGO_IMAGE) {
                        $value = $this->wallet->logo_image_url;
                    }

                    if ($optionInstance->optionName === WalletPropertyType::PUSH_NOTIFICATION_ICON) {
                        $value = $this->wallet->push_notification_icon_url;
                    }
                }

                return [$optionInstance->optionName => $value];
            }
        });
    }

    public function value($optionName)
    {
        $option = $this->option($optionName);

        return $option && $option->value ? $option->value : null;
    }

    public function getOptionModel($optionName)
    {
        if (!$this->wallet) {
            throw new BadRequestException('Не указан wallet шаблон для работы с опциями'); // @todo BadRequestException
        }

        return $this->wallet->options()->whereHas('walletPropertyType', function ($query) use ($optionName) {
            return $query->where('name', $optionName);
        })->first();
    }

    public function imageOption($optionName)
    {
        $optionValue = $this->value($optionName);

        if (!$optionValue) {
            return null;
        }

        return Storage::disk('wallet_options')->exists($this->wallet->id . '/' . $optionValue)
            ? Storage::disk('wallet_options')->get($this->wallet->id . '/' . $optionValue)
            : null;
    }

    public function downloadImageOption($optionName)
    {
        $optionValue = $this->value($optionName);

        if (!$optionValue) {
            throw new BadRequestException('Image not found'); //@ todo BadRequestException
        }

        return Storage::disk('wallet_options')->download($this->wallet->id . '/' . $optionValue);
    }
}