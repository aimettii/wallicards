<?php

namespace App\Services;

use App\Models\Client;
use App\Models\WalletPropertyType;
use App\Models\WalletPropertyValues;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Passbook\Pass\Barcode;
use Passbook\Pass\Field;
use Passbook\Pass\Image;
use Passbook\Pass\Location;
use Passbook\Pass\Structure;
use Passbook\PassFactory;
use Passbook\PassValidator;
use Passbook\Type\StoreCard;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class WalletPass
{
    public $client;
    public $shortCodes;
    public $passName;
    public $passWasExist = false;
    public $pushMessage;

    const ANDROID_REQUEST_URL = 'https://walletpasses.appspot.com/api/v1/push';
    const WALLET_PASSES_API_KEY = '10db178a234f4b57b5deee86b98449ea';

    public function setClient(Client $client)
    {
        $this->client = $client;
        $this->shortCodes = $this->generatedShortCodes();

        if ($this->client->pkpass_file) {
            $this->passName = $this->client->pkpass_file;

            if (file_exists($this->pkpassFile())) {
                $this->passWasExist = true;
            } else {
                $this->client->updatePassFile(null);
                $this->passName = $this->generateNewPassName();
            }

        } else {
            $this->passName = $this->generateNewPassName();
        }

        return $this;
    }

    public function generateNewPassName()
    {
        return md5($this->client->serial_number);
    }

    public function generatePass()
    {
        if (\App::environment('local')) {
            throw new BadRequestException('Запрет генерации pkpass на locale'); // @todo BadRequestException
        }

        $client = $this->getClient();

        $wallet = $client->wallet;
        $walletOptions = \WalletOptions::setWalletInstance($wallet);

        $pass = new StoreCard($client->serial_number, $wallet->name);

        $backgroundColor = $walletOptions->option(WalletPropertyType::BACKGROUND_COLORS);

        /** устанавливаем цвет бэкграунда */
        if ($backgroundColor) {
            $pass->setBackgroundColor($backgroundColor->value);
        }

        $fontColor = $walletOptions->option(WalletPropertyType::VALUE_COLORS);

        /** устанавливаем цвет шрифта */
        if ($fontColor) {
            $pass->setForegroundColor($fontColor->value);
        }

        $labelColor = $walletOptions->option(WalletPropertyType::TITLE_COLORS);

        /** устанавливаем цвет шрифта лейблов */
        if ($labelColor){
            $pass->setLabelColor($labelColor->value);
        }

        $geolocation = $walletOptions->option(WalletPropertyType::GEOLOCATION);

        if ($geolocation) {
            $location = new Location($geolocation->value->latitude, $geolocation->value->longitude);
            $location->setRelevantText($geolocation->value->message);
            $pass->addLocation($location);
        }

        $logoText = $walletOptions->option(WalletPropertyType::LOGO_TEXT);

        /** устанавливаем текст рядом с лого */
        if ($logoText) {
            $pass->setLogoText($logoText->generateShortCodes($this->shortCodes)->value);
        }

        $structure = new Structure();

        $primaryFields = $walletOptions->option(WalletPropertyType::PRIMARY_FIELDS);

        if ($primaryFields) {
            $primaryField = $primaryFields->first();
            $primary = new Field('PrimaryField', $primaryField->generateShortCodes($this->shortCodes)->value->value);
            $primary->setLabel($primaryField->value->label);
            $structure->addPrimaryField($primary);
        }

        $secondaryFields = $walletOptions->option(WalletPropertyType::SECONDARY_FIELDS);

        if ($secondaryFields) {
            $secondaryField = $secondaryFields->first();

            $secondary = new Field('SecondaryField', $secondaryField->generateShortCodes($this->shortCodes)->value->value);
            $secondary->setLabel($secondaryField->value->label);
            $structure->addSecondaryField($secondary);
        }

        $auxiliaryFields = $walletOptions->option(WalletPropertyType::AUXILIARY_FIELDS);

        if ($auxiliaryFields) {
            $auxiliaryField = $auxiliaryFields->first();
            $auxiliary = new Field('AuxiliaryField', $auxiliaryField->generateShortCodes($this->shortCodes)->value->value);
            $auxiliary->setLabel($auxiliaryField->value->label);
            $structure->addAuxiliaryField($auxiliary);
        }

        $headerFields = $walletOptions->option(WalletPropertyType::HEADER_FIELDS);

        if ($headerFields) {
            $headerField = $headerFields->first();
            $header = new Field('HeaderField', $headerField->generateShortCodes($this->shortCodes)->value->value);
            $header->setLabel($headerField->value->label);
            $structure->addHeaderField($header);
        }

//        $changeMessage = $walletOptions->option(WalletPropertyType::CHANGE_MESSAGE);

        $backFields = $walletOptions->option(WalletPropertyType::BACK_FIELDS);

        /** message push */
        $push = new Field('message', $this->pushMessage ? $this->pushMessage : '');
        $push->setChangeMessage('%@')->setTextAlignment(Field::ALIGN_LEFT);
        $firstPushBackField = $backFields->first();
        if ($backFields->count() > 1 && ($firstPushBackField->value->value === null || $firstPushBackField->value->value === 'null')) {
            $push->setLabel($firstPushBackField->value->label);
            $backFields = $backFields->splice(1);
        }

        $structure->addBackField($push);

        /** Текст на обратной стороне */
        if ($backFields) {
            foreach ($backFields as $backField) {
                $BackField = new Field('BackField', $backField->generateShortCodes($this->shortCodes)->value->value);
                $BackField->setLabel($backField->value->label);
                $structure->addBackField($BackField);
            }
        }

        $useFeedBackSystem = $walletOptions->value(WalletPropertyType::USE_FEEDBACK_SYSTEM);

        if ($useFeedBackSystem) {
            $BackField = new Field('BackFields', "<a href='wallicards.ru'>Оставить отзыв</a>");
            $BackField->setLabel('Обратная связь');
            $structure->addBackField($BackField);
        }

        $BackField = new Field('BackFields', "<a href='wallicards.ru'>wallicards.ru</a>");
        $BackField->setLabel('Эмитент');
        $structure->addBackField($BackField);

        $iconImage = $walletOptions->option(WalletPropertyType::PUSH_NOTIFICATION_ICON);

        // Add icon image
        if ($iconImage) {
            $pass->addImage(new Image(storage_path('app/wallet_options/' . $wallet->id . '/' . $iconImage->value), 'icon'));
        }

        $logoImage = $walletOptions->option(WalletPropertyType::LOGO_IMAGE);

        if ($logoImage) {
            $pass->addImage(new Image(storage_path('app/wallet_options/' . $wallet->id . '/' . $logoImage->value), 'logo'));
        }

        $backgroundImage = $walletOptions->option(WalletPropertyType::BACKGROUND_IMAGE);

        if ($backgroundImage) {
            $pass->addImage(new Image(storage_path('app/wallet_options/' . $wallet->id . '/' . $backgroundImage->value), 'strip'));
        }

        /****
         **
        background.png	Фоновая картинка для карты.
        icon.png	Иконка для уведомлений и писем
        logo.png	Логотип карточки. Отображается слева сверху
        strip.png	Картинка, находящаяся сзади основного описания карточки
         ***/

        // Set pass structure
        $pass->setStructure($structure);

        $barcode = $walletOptions->option(WalletPropertyType::CODE_SCAN_TYPE);

        // Add barcode
        //
        $barcode = new Barcode($barcode->value, $client->serial_number);

        $numberUnderCode = $walletOptions->option(WalletPropertyType::NUMBER_CARD_UNDER_CODE);

        if ($numberUnderCode && $numberUnderCode->value) {
            $barcode->setAltText($client->serial_number);
        }

        $pass->setBarcode($barcode);

        $pass->setSerialNumber($client->serial_number);

        $pass->setAuthenticationToken(bcrypt($client->serial_number));
        $pass->setWebServiceURL(getenv('PRODUCTION_URL'));

        $certificate = $wallet->certificate;

        $factory = new PassFactory(
            $certificate->pass_type_identifier,
            $certificate->team_identifier,
            $certificate->organization_name,
            $certificate->p12_path,
            $certificate->p12_password,
            $certificate->wwdr_path
        );

        $factory->setOutputPath($this->pkpassDirectory());
        $factory->setOverwrite(true);
        $factory->package($pass, $this->passName);

        $validator = new PassValidator();
        $validator->validate($pass);

        if ($validator->getErrors()) {
            $errorsString = implode(' | ', $validator->getErrors());
            throw new \Exception("Ошибки валидации passbook: $errorsString"); // @todo Exception
        }

        if (file_exists($this->pkpassFile())) {
            $client->updatePassFile($this->passName);

            return $this->passName;
        } else {
            throw new FileException("Не можем сохранить pkpass файл");
        }
    }

    public function getClient()
    {
        if (! $this->client) {
            throw new BadRequestException('Не указан клиент для работы с walletPass'); // @todo BadRequestException
        }

        return $this->client;
    }

    /**
     * @param $pushMessage
     * @throws \Exception
     */
    public function sendPushMessage($pushMessage)
    {
        if (\App::environment('local')) {
            throw new BadRequestException('Запрет генерации pkpass на locale'); // @todo BadRequestException
        }

        $client = $this->getClient();

        if ($client->status_on_device !== Client::STATUS_DEVICE_ACTIVE
            || $client->platform === null
            || $client->push_token === null
        ) {
            throw new \Exception('Клиент не активирован для отправки PUSH сообщений');
        }

//        $wallet = $client->wallet;
//
//        $walletOptions = \WalletOptions::setWalletInstance($wallet);
//
//        $changeMessage = $walletOptions->getOptionModel(WalletPropertyType::CHANGE_MESSAGE);
//
//        $changeMessage->wallet_property_value = WalletPropertyValues::formatArrValueFromOption($pushMessage, WalletPropertyType::CHANGE_MESSAGE, $wallet); // @todo обновление опции
//
//        $changeMessage->save();

        $this->pushMessage = $pushMessage;

        $this->generatePass();

        return $this->sendPushRequest();
    }

    public function pkpassFile()
    {
        if (! $this->passName) {
            throw new BadRequestException('Не указан passName для работы с pkpassFile'); // @todo BadRequestException
        }

        return $this->pkpassDirectory() . '/' . $this->pkpassFilename();
    }

    public function pkpassFilename()
    {
        if (! $this->passName) {
            throw new BadRequestException('Не указан passName для работы с pkpassFilename'); // @todo BadRequestException
        }

        return $this->passName . '.pkpass';
    }

    public function pkpassDirectory()
    {
        if (! $this->client) {
            throw new BadRequestException('Не указан клиент для работы с pkpassDirectory'); // @todo BadRequestException
        }

        return $this->client->clientDirectory();
    }

    public function generatedShortCodes()
    {
        if (! $this->client) {
            throw new BadRequestException('Не указан клиент для работы с walletPass'); // @todo BadRequestException
        }

        return [
            'client_name' => $this->client->name,
            'client_date_of_birth' => $this->client->date_of_birth->format('d.m.Y'),
            'client_balance' => $this->client->balance,
            'client_serial' => $this->client->serial_number,
            'client_status' => $this->client->status ? $this->client->status : '',
            'client_last_visit_day' => $this->client->last_visit ? $this->client->last_visit->format('d.m.Y H:s:i') : '',
            'client_created_at_date' => $this->client->created_at ? $this->client->created_at->format('d.m.Y') : '',
            'client_cashback' => !is_null($this->client->cashback) ? $this->client->cashback . '%' : '',
            'client_discount' => !is_null($this->client->discount) ? $this->client->discount . '%' : '',
            'client_points_spent' => $this->client->points_spent,
            'client_last_amount_purchase' => !is_null($this->client->last_transaction_amount_purchase) ? $this->client->last_transaction_amount_purchase : '',
        ];
    }

    public function sendPushRequest() {
        $client = $this->getClient();

        if ($client->platform === Client::PLATFORM_ANDROID) {
            return $this->sendPushMessageAndroidRequest();
        } else if ($client->platform === Client::PLATFORM_IOS) {
            return $this->sendPushMessageAppleRequest();
        }

        return false;
    }

    private function sendPushMessageAppleRequest() {
        $client = $this->getClient();

        $certificate = $client->wallet->certificate;

        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'passphrase', $certificate->p12_password);
        stream_context_set_option($ctx, 'ssl', 'local_cert', $certificate->push_path);
        $fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT, $ctx);
        if (!$fp) {
            Log::debug("Failed to connect (stream_socket_client): $err $errstr");
        } else {
            stream_set_blocking($fp, 0);
            $body = [];
            $body['aps'] = [];
            $payload = json_encode($body);
            $msg = chr(0) . pack('n', 32) . pack('H*', $client->push_token) . pack('n', strlen($payload)) . $payload;
            $result = fwrite($fp, $msg);
            fclose($fp);
        }
    }

    private function sendPushMessageAndroidRequest() {
        $client = $this->getClient();

        $certificate = $client->wallet->certificate;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, self::ANDROID_REQUEST_URL);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode([
            'passTypeIdentifier' => $certificate->pass_type_identifier,
            'pushTokens' => [$client->push_token]
        ]));
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Authorization: ' . self::WALLET_PASSES_API_KEY,
            'Accept: application/json',
            'Content-type: application/json'
        ]);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $out = curl_exec($curl);
        curl_close($curl);
        Log::debug($out);
        return $out;
    }
}