<?php

namespace App\Services;

use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class SMS
{
    const API_SMS_RU = '242EAFBD-FAF5-BC11-2427-265B38DB3B2D';

    public $phone;
    public $message;

    public function phone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    public function message($msg)
    {
        $this->message = $msg;

        return $this;
    }

    public function send()
    {
        if (!$this->phone || !$this->message) {
            throw new BadRequestException('Phone or message should be setted before sending SMS'); // @todo BadRequestException
        }

        try {
            $apiToken = self::API_SMS_RU;
            $phone = $this->phone;

            $ch = curl_init("https://sms.ru/sms/send");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
                "api_id" => $apiToken,
                "to" => $phone, // До 100 штук до раз
                "msg" => iconv("windows-1251", "utf-8", $this->message), // Если приходят крякозябры, то уберите iconv и оставьте только "Привет!",
                "json" => 1 // Для получения более развернутого ответа от сервера
            )));
            $body = curl_exec($ch);
            curl_close($ch);

            $json = json_decode($body);

            if ($json) { // Получен ответ от сервера
                if ($json->status == "OK") { // Запрос выполнился
                    foreach ($json->sms as $phone => $data) { // Перебираем массив СМС сообщений
                        if ($data->status == "OK") { // Сообщение отправлено
                            return true;
                        } else { // Ошибка в отправке
                            throw new BadRequestException('Error');
                        }
                    }
                } else {
                    throw new BadRequestException('Error');
                }
            } else {
                throw new BadRequestException('Error');
            }
        } catch (\Exception $e) {
            Log::error("An error occurred while sending sms SMS.ru\n" . $body);
            throw new BadRequestException('An error occurred while sending sms'); // @todo BadRequestException
        }
    }
}