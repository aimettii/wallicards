<?php


namespace App\Services;

use Illuminate\Support\Collection;
use Illuminate\Validation\ValidationException;
use Validator;

class RequestListParameters
{
    const Q_COMPARISON_STRICT_TYPE = 'strict';
    const Q_COMPARISON_LIKE_TYPE = 'like';

    const SORT_DIR_DESC = 'desc';
    const SORT_DIR_ASC = 'asc';

    protected $options;

    public function __construct(array $options)
    {
        $this->setOptions($options);
    }

    /**
     * Get options
     *
     * @param null $key
     * @param null $default
     * @return mixed
     */
    public function getOptions($key = null, $default = null)
    {
        $options = $this->keysCollectionHelper($this->options, $key);

        return $options ?? $default;
    }

    /**
     * Set options
     *
     * @param array $options
     */
    public function setOptions(array $options): void
    {
        $parsedOptions = [];

        foreach ($options as $keyOption => $option) {

            if (strpos($keyOption, 'q.') === 0) {
//                $qParts = explode('.', $keyOption);
//                $qProperty = $qParts[1] ?: null;
//
//                if ($qProperty) {
//                    $qPropertyRelations = '';
//
//                    if (count($qParts) > 2) {
//                        foreach ($qParts as $qPartKey => $qPartValue) {
//                            if ($qPartKey > 1) {
//                                $qPropertyRelations .= `$qPartValue` . (count($qParts) === $qPartKey) ? '' : '.';
//                            }
//                        }
//                    }
//
//                    if (strpos($option, '!') === 0) {
//                        $qComparisonType = self::Q_COMPARISON_STRICT_TYPE;
//                        $qValue = substr_replace($option, '', 0, 1);
//                    } else if (strpos($option, '+') === 0) {
//                        $qComparisonType = self::Q_COMPARISON_LIKE_TYPE;
//                        $qValue = substr_replace($option, '', 0, 1);
//                    } else {
//                        $qComparisonType = self::Q_COMPARISON_STRICT_TYPE;
//                        $qValue = $option;
//                    }
//
//                    $qSettings = [
//                        'property' => $qProperty,
//                        'value' => $qValue,
//                        'comparison_type' => $qComparisonType
//                    ];
//
//                    if (! empty($qPropertyRelations)) {
//                        $qSettings['relations'] = $qPropertyRelations;
//                    }
//
//                    $parsedOptions['q'][] = $qSettings;
//                }
            } else {

                switch ($keyOption) {
                    case 'sort':
                        if (strpos($option, '-') === 0) {
                            $sortDir = self::SORT_DIR_DESC;
                            $sortField = substr_replace($option, '', 0, 1);
                        } else {
                            $sortDir = self::SORT_DIR_ASC;
                            $sortField = $option;
                        }

                        $sortSettings = [
                            'property' => $sortField,
                            'dir' => $sortDir
                        ];

                        $parsedOptions['sort'] = $sortSettings;
                        break;
                    case 'q':
                        $parsedOptions['q'] = $option;
                        break;
                    case 'pagination':
                        $parsedOptions['pagination'] = $option;
                        break;
                    case 'per_page':
                        $parsedOptions['per_page'] = $option;
                        break;
                }

            }
        }

        $this->options = collect($parsedOptions);
    }

    /**
     * Has options
     *
     * @param string $key
     * @return mixin
     */
    public function hasOptions(string $key)
    {
        return $this->options->has($key);
    }

    /**
     * Check if pagination options exists
     *
     * @return bool
     * @throws ValidationException
     */
    public function hasPagination()
    {
        if ($this->hasOptions('per_page') || filter_var($this->getOptions('pagination'), FILTER_VALIDATE_BOOLEAN) === true) {
            $validator = Validator::make($this->getPaginationOptions()->toArray(), [
                'per_page' => 'integer|min:1',
                'page' => 'integer|min:1',
            ]);

            if ($validator->fails()) {
                throw new ValidationException($validator);
            }

            return true;
        }

        return false;
    }

    public function hasFilter()
    {
        if ($this->hasOptions('q') && is_array($this->getOptions('q')) && ! empty($this->getOptions('q'))) {
            return true;
        }

        return false;
    }

    public function getFilter()
    {
        return $this->hasFilter() ? $this->getOptions('q') : null;
    }

    /**
     * Get pagination repository
     *
     * @param null $keys
     * @return mixed
     * @throws ValidationException
     */
    public function pagination($keys = null)
    {
        if ($this->hasPagination())
        {
            return $this->keysCollectionHelper($this->getPaginationOptions(), $keys);
        }

        return null;
    }

    /**
     * Get pagination options
     *
     * @return Collection
     */
    private function getPaginationOptions()
    {
        return $this->getOptions()->only(['per_page', 'page']);
    }

    /**
     * Transform options by array keys or key
     *
     * @param Collection $options
     * @param null|array|string $key
     * @return mixed
     */
    private function keysCollectionHelper(Collection $options, $key = null)
    {
        return is_array($key) ? $options->only($key) :
            ( is_string($key)
                ? $options->get($key)
                : $options
            );
    }
}
