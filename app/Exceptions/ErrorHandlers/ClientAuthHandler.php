<?php


namespace App\Exceptions\ErrorHandlers;


use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class ClientAuthHandler extends ErrorHandler
{
    const NOT_FOUND_VERIFICATION_SMS_SENT = 10004;
    const TOO_FREQUENT_REQUESTS = 10003;
    const INCORRECT_CODE = 10005;
    const ERROR_SENDING_SMS = 10006;
    const TOO_MANY_VERIFICATION_ATTEMPTS = 10007;
    const TOO_MANY_SMS_SENT = 10008;
    const NOT_VERIFIED = 10009;
    const CLIENT_NOT_FOUND = 10010;

    public static $settings = [
        self::NOT_FOUND_VERIFICATION_SMS_SENT => [
            'message' => 'An error occurred while verification client.',
            'description' => 'Did not find verification sent SMS.',
        ],
        self::TOO_FREQUENT_REQUESTS => [
            'message' => 'Too frequent requests while sending sms.',
            'description' => 'Please try again after 1 min.',
        ],
        self::TOO_MANY_SMS_SENT => [
            'message' => 'Too many sms sent.',
            'description' => 'Please try again after 5 min.',
        ],
        self::INCORRECT_CODE => [
            'message' => 'Incorrect code.',
            'description' => 'Please try again.',
        ],
        self::ERROR_SENDING_SMS => [
            'message' => 'An error occurred while sending sms.',
            'description' => 'Server-side error occurred.',
        ],
        self::TOO_MANY_VERIFICATION_ATTEMPTS => [
            'message' => 'An error occurred while verify client.',
            'description' => 'Too many verification attempts.',
        ],
        self::NOT_VERIFIED => [
            'message' => 'An error occurred while verify client.',
            'description' => 'Client should be verified.',
        ],
        self::CLIENT_NOT_FOUND => [
            'message' => 'An error occurred while founding client.',
            'description' => 'Client not found.',
        ],

    ];

    public static function httpCodesScheme()
    {
        return [
            429 => [ // Много попыток запросов
                self::TOO_FREQUENT_REQUESTS,
                self::TOO_MANY_VERIFICATION_ATTEMPTS,
                self::TOO_MANY_SMS_SENT,
            ],
            403 => [ // Отказано в доступе
                self::INCORRECT_CODE,
                self::NOT_VERIFIED,
            ],
            404 => [ // Не найдено
                self::NOT_FOUND_VERIFICATION_SMS_SENT,
                self::CLIENT_NOT_FOUND,
            ],
            500 => [ // Ошибка на стороне сервера
                self::ERROR_SENDING_SMS
            ],

        ];
    }

    public static function code(int $code) {
        foreach (self::httpCodesScheme() as $httpCode => $schemeItem) {
            if (in_array($code, $schemeItem)) {
                return $httpCode;
            }
        }

        return self::BASE_HTTP_CLIENT_CODE;
    }

    public static function message(int $code)
    {
        $settings = isset(self::$settings[$code])
            ? self::$settings[$code]
            : (
            isset(parent::$settings[$code])
                ? parent::$settings[$code]
                : null
            );

        if (is_null($settings)) {
            throw new BadRequestException('Вы должны настроить ошибку: ' . $code); //@todo BadRequestException
        }

        return isset($settings['message']) ? $settings['message'] : null;
    }

    public static function description(int $code)
    {
        $settings = isset(self::$settings[$code])
            ? self::$settings[$code]
            : (
            isset(parent::$settings[$code])
                ? parent::$settings[$code]
                : null
            );

        if (is_null($settings)) {
            throw new BadRequestException('Вы должны настроить ошибку: ' . $code); //@todo BadRequestException
        }

        return isset($settings['description']) ? $settings['description'] : null;
    }
}