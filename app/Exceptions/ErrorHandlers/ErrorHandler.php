<?php


namespace App\Exceptions\ErrorHandlers;


abstract class ErrorHandler
{
    const BASE_HTTP_CLIENT_CODE = 400;

    const VALIDATION_CODE = 10002;
    const TOO_FREQUENT_REQUESTS = 10003;

    public static $settings = [
        self::TOO_FREQUENT_REQUESTS => [
            'message' => 'To frequent requests.',
            'description' => 'Please try again later.',
        ]
    ];

    abstract public static function message(int $code);

    abstract public static function description(int $code);

}