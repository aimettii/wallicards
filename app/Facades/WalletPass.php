<?php


namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class WalletPass extends Facade
{
    protected static function getFacadeAccessor() { return 'wallet_pass'; }
}