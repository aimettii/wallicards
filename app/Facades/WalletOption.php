<?php


namespace App\Facades;


use Illuminate\Support\Facades\Facade;

class WalletOption extends Facade
{
    protected static function getFacadeAccessor() { return 'wallet_options'; }
}