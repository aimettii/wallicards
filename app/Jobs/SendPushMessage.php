<?php

namespace App\Jobs;

use App\Facades\WalletPass;
use App\Models\Push;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendPushMessage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $push;
    public $pushMessage;

    public $tries = 3;

    /**
     * Create a new job instance.
     *
     * @param Push $push
     * @param string $pushMessage
     */
    public function __construct(Push $push, string $pushMessage)
    {
        $this->push = $push;
        $this->pushMessage = $pushMessage;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        foreach ($this->push->clients as $client) {
            try {
                WalletPass::setClient($client)->sendPushMessage($this->pushMessage);
                $this->push->clients()->updateExistingPivot($client->id, ['status' => Push::STATUS_PERFORMED]);
            } catch(\Exception $e) {
//                if ($this->attempts() < $this->tries - 1) {
//                    $delayInSeconds = 15; // 30 сек
//                    $this->release($delayInSeconds);
//                } elseif ($this->attempts() == $this->tries - 1){
//                    $delayInSeconds = 30; // сек
//                    $this->release($delayInSeconds);
//                } else {
//                    $this->push->clients()->updateExistingPivot($client->id, ['status' => Push::STATUS_FAILED]);
//                    $this->fail($e);
//                    throw new \Exception($e);
//                }

                $this->push->clients()->updateExistingPivot($client->id, ['status' => Push::STATUS_FAILED]);
//                $this->fail($e);
//                throw new \Exception($e);
            }
        }
    }
}
