<?php


namespace App\Contracts;

use App\Models\Client;

interface WalletPass
{
    public function setClient(Client $client) : self;

    public function generatePass();
}