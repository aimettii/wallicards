<?php

namespace App\Http\Requests\API\Company;

use Illuminate\Foundation\Http\FormRequest;

class CreatePushAPIRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'message' => 'required|string',
            'will_be_executed_at' => 'nullable|date|after:now',
            'clients_ids' => 'required|array',
            'clients_ids.*' => 'required|integer|exists:clients,id',
        ];
    }
}
