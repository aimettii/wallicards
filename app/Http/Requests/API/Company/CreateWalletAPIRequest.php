<?php

namespace App\Http\Requests\API\Company;

use App\Http\Requests\API\BaseAPIRequest;
use App\Models\Wallet;
use App\Models\WalletPropertyType;
use App\Models\WalletType;
use Illuminate\Validation\Rule;
use Passbook\Pass\Barcode;

class CreateWalletAPIRequest extends BaseAPIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function baseRules()
    {
        return [
            'wallet_type' => 'required|exists:wallet_types,name',
            'name' => 'required|string|min:1',
            'options' => 'required|array',
            'certificate_id' => [
                'required',
                Rule::exists('wallet_certificates', 'id')->where(function ($query) {
                    $query->where('company_id', $this->user()->company->id);
                }),
            ]
        ];
    }

    public function uniqueTypeRules()
    {
        return [
            WalletPropertyType::GEOLOCATION => 'array',
            WalletPropertyType::GEOLOCATION_MESSAGE => [
                Rule::requiredIf(!empty($this->getOption(WalletPropertyType::GEOLOCATION))),
                'string'
            ],
            WalletPropertyType::GEOLOCATION_LATITUDE => [
                Rule::requiredIf(!empty($this->getOption(WalletPropertyType::GEOLOCATION))),
                'numeric',
                'min:0',
                'max:100',
            ],
            WalletPropertyType::GEOLOCATION_LONGITUDE => [
                Rule::requiredIf(!empty($this->getOption(WalletPropertyType::GEOLOCATION))),
                'numeric',
                'min:0',
                'max:100',
            ],
            WalletPropertyType::PUSH_NOTIFICATION_ICON => 'required|mimes:jpeg,png',
            WalletPropertyType::LOGO_IMAGE => 'required|mimes:jpeg,png',
            WalletPropertyType::BACKGROUND_IMAGE => 'required|mimes:jpeg,png',
            WalletPropertyType::BACKGROUND_COLORS => 'required|string', //@TODO RGB color validate
            WalletPropertyType::TITLE_COLORS => 'required|string', //@TODO RGB color validate
            WalletPropertyType::VALUE_COLORS => 'required|string', //@TODO RGB color validate
            WalletPropertyType::CODE_SCAN_TYPE => 'required|in:' . implode(',', [Barcode::TYPE_QR, Barcode::TYPE_CODE_128]),
            WalletPropertyType::NUMBER_CARD_UNDER_CODE => 'required|boolean',
            WalletPropertyType::USE_FEEDBACK_SYSTEM => 'required|boolean',

            WalletPropertyType::LOGO_TEXT => 'string', // @todo Содержимое поле может содержать шорт-коды {...} validate
            WalletPropertyType::CHANGE_MESSAGE => 'string',

            WalletPropertyType::PRIMARY_FIELDS => 'array',
            WalletPropertyType::PRIMARY_FIELDS_LABEL => [
                Rule::requiredIf(!empty($this->getOption(WalletPropertyType::PRIMARY_FIELDS))),
                'string'
            ],
            WalletPropertyType::PRIMARY_FIELDS_VALUE => [
                Rule::requiredIf(!empty($this->getOption(WalletPropertyType::PRIMARY_FIELDS))),
                'string' // @todo Содержимое поле может содержать шорт-коды {...} validate
            ],

            WalletPropertyType::SECONDARY_FIELDS => 'array',
            WalletPropertyType::SECONDARY_FIELDS_LABEL => [
                Rule::requiredIf(!empty($this->getOption(WalletPropertyType::SECONDARY_FIELDS))),
                'string'
            ],
            WalletPropertyType::SECONDARY_FIELDS_VALUE => [
                Rule::requiredIf(!empty($this->getOption(WalletPropertyType::SECONDARY_FIELDS))),
                'string' // @todo Содержимое поле может содержать шорт-коды {...} validate
            ],

            WalletPropertyType::AUXILIARY_FIELDS => 'array',
            WalletPropertyType::AUXILIARY_FIELDS_LABEL => [
                Rule::requiredIf(!empty($this->getOption(WalletPropertyType::AUXILIARY_FIELDS))),
                'string'
            ],
            WalletPropertyType::AUXILIARY_FIELDS_VALUE => [
                Rule::requiredIf(!empty($this->getOption(WalletPropertyType::AUXILIARY_FIELDS))),
                'string' // @todo Содержимое поле может содержать шорт-коды {...} validate
            ],

            WalletPropertyType::HEADER_FIELDS => 'array',
            WalletPropertyType::HEADER_FIELDS_LABEL => [
                Rule::requiredIf(!empty($this->getOption(WalletPropertyType::HEADER_FIELDS))),
                'string'
            ],
            WalletPropertyType::HEADER_FIELDS_VALUE => [
                Rule::requiredIf(!empty($this->getOption(WalletPropertyType::HEADER_FIELDS))),
                'string' // @todo Содержимое поле может содержать шорт-коды {...} validate
            ],

            WalletPropertyType::BACK_FIELDS => 'array',
            WalletPropertyType::BACK_FIELDS_LABEL => [
                Rule::requiredIf(!empty($this->getOption(WalletPropertyType::BACK_FIELDS))),
                'string'
            ],
            WalletPropertyType::BACK_FIELDS_VALUE => [
                Rule::requiredIf(!empty($this->getOption(WalletPropertyType::BACK_FIELDS))),
                'string' // @todo Содержимое поле может содержать шорт-коды {...} validate
            ],
        ];
    }

    public function discountTypeRules()
    {
        return [
//            WalletPropertyType::DISCOUNT => 'required|numeric|between:0,1', // @todo Between validation %discount
            WalletPropertyType::DISCOUNT => 'required|integer|between:0,100',
        ];
    }

    public function bonusTypeRules()
    {
        return [
//            WalletPropertyType::CASHBACK => 'required|numeric|between:0,1',
            WalletPropertyType::CASHBACK => 'required|integer|between:0,100',
            WalletPropertyType::CONVERSION_POINTS => 'required|integer',
//            WalletPropertyType::CONVERSION_POINTS => 'required|numeric|between:0,1',

            WalletPropertyType::WELCOME_POINTS => 'array',
            WalletPropertyType::WELCOME_POINTS_COUNT
            => [
                Rule::requiredIf(!empty($this->getOption(WalletPropertyType::WELCOME_POINTS))),
                'integer', // @todo Between validation
                'min:1', // @todo Between validation'
            ],
            WalletPropertyType::WELCOME_POINTS_SUM_SPENDING
            => [
                'exclude_if:options.' . WalletPropertyType::WELCOME_POINTS_IMMEDIATELY . ',true',
                Rule::requiredIf(
                    (
                        !empty($this->getOption(WalletPropertyType::WELCOME_POINTS)) &&
                        !$this->getOption(WalletPropertyType::WELCOME_POINTS_IMMEDIATELY)
                    )
                ),
                'integer', // @todo Between validation
                'min:1', // @todo Between validation
            ],
            WalletPropertyType::WELCOME_POINTS_IMMEDIATELY => [
                Rule::requiredIf(!empty($this->getOption(WalletPropertyType::WELCOME_POINTS))),
                'boolean'
            ],

            WalletPropertyType::BURNING_POINTS => 'array',
            WalletPropertyType::BURNING_POINTS_DAYS_LIMIT => [
                Rule::requiredIf(!empty($this->getOption(WalletPropertyType::BURNING_POINTS))),
                'integer', // @todo Min days validation
                'min:1', // @todo Min days validation
            ]
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = self::baseRules();

        switch ($this->wallet_type) {
            case Wallet::TYPE_DISCOUNT:
                $rules = array_merge($rules, self::optionsRuleFormatting(self::discountTypeRules()));
                break;
            case Wallet::TYPE_BONUS:
                $rules = array_merge($rules, self::optionsRuleFormatting(self::bonusTypeRules()));
                break;
            case Wallet::TYPE_COUPON:

                break;
            case Wallet::TYPE_CHOP:

                break;
            default:
                throw new \Error("Undefined option");
                break;
        }

        $walletType = WalletType::where('name', $this->wallet_type)->firstOrFail();

        $this->merge([
            'wallet_type_id' => $walletType->id
        ]);

        $rules = array_merge($rules, self::optionsRuleFormatting(self::uniqueTypeRules()));

        return $rules;
    }

    public function getOption($key)
    {
        return $this->input('options.' . $key);
    }

    protected static function optionsRuleFormatting(array $rules)
    {
        return collect($rules)->mapWithKeys(function ($rule, $ruleKey) {
            return ['options.' . $ruleKey => $rule];
        })->all();
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
//        $type = WalletType::find((int)$this->wallet_type_id);
//
//        if (! $type) {
//            return;
//        }
//
//        $this->merge([
//            'wallet_type' => $type->name
//        ]);
    }
}
