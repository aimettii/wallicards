<?php


namespace App\Http\Requests\API\Company;

use App\Http\Requests\API\BaseAPIRequest;
use App\Models\Client;

class CreateClientAPIRequest extends BaseAPIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|string',
            'gender' => 'required|in:' . implode(',', Client::defaultGenders()),
            'date_of_birth' => 'required|date',
            'phone'     => 'required|digits:11|regex:/7[\d]{10,}/',
            'wallet_id' => 'required|exists:wallets,id',
            'balance' => 'nullable|numeric|min:0',
        ];
    }
}