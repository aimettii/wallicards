<?php


namespace App\Http\Requests\API\Company;


use App\Http\Requests\API\BaseAPIRequest;
use App\Models\Client;

class UpdateClientAPIRequest extends BaseAPIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'string',
            'gender' => 'in:' . implode(',', Client::defaultGenders()),
            'date_of_birth' => 'date',
            'balance' => 'numeric|min:0',
            'favorite' => 'boolean'
        ];
    }
}