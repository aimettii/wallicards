<?php

namespace App\Http\Requests\API;

use App\Models\Company;

class CreatePartnerAPIRequest extends BaseAPIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'bail|required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
            'phone' => 'required|string|unique:phones,phone_number', // @todo use Validation phones in App\Models\User@createPhones()
            'company.name' => 'required|string',
            'company.address' => 'required|string',
            'company.note' => 'nullable|string',
            'company.status' => [
                'string',
                self::ruleInFormatted(Company::getAllStatuses())
            ],
        ];
    }
}
