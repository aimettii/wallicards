<?php

namespace App\Http\Requests\API\Client;

use App\Models\Client;
use App\Http\Requests\API\BaseAPIRequest;

class RegisterClientAPIRequest extends BaseAPIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'gender' => 'required|in:' . implode(',', Client::defaultGenders()),
            'date_of_birth' => 'required|date',
            'phone'     => 'required|digits:11|regex:/7[\d]{10,}/',
        ];
    }
}
