<?php

namespace App\Http\Requests\API;

use App\Models\Company;

class UpdatePartnerAPIRequest extends BaseAPIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string',
            'email' => 'email|unique:users,email',
            'password' => 'string',
            'phone' => 'string', // @todo use Validation phones in App\Models\User@createPhones()
            'company.name' => 'string',
            'company.address' => 'string',
            'company.note' => 'nullable|string',
            'company.status' => [
                'string',
                self::ruleInFormatted(Company::getAllStatuses())
            ],
        ];
    }
}
