<?php

namespace App\Http\Requests\API\Partner;

use App\Http\Requests\API\BaseAPIRequest;
use App\Models\Employee;
use App\Models\Permission;

class CreateEmployeeAPIRequest extends BaseAPIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'bail|required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
            'phone' => 'required|digits:11|regex:/7[\d]{10,}/', // @todo use Validation phones in App\Models\User@createPhones()
            'note' => 'nullable|string',
            'permissions' => 'present|array',
            'permissions.*' => 'string|' . self::ruleInFormatted(Permission::employeePermissions()),
            'send_info_mail' => 'nullable|boolean'
//            'roles' => 'required|min:1|' . self::ruleInFormatted(),
//            'permissions' => 'required|min:1|' . self::ruleInFormatted(),
        ];
    }
}
