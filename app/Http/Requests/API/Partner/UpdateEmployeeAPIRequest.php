<?php

namespace App\Http\Requests\API\Partner;

use App\Http\Requests\API\BaseAPIRequest;
use App\Models\Permission;

class UpdateEmployeeAPIRequest extends BaseAPIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string',
            'email' => 'email',
            'password' => 'string',
            'phone' => 'digits:11|regex:/7[\d]{10,}/', // @todo use Validation phones in App\Models\User@createPhones()
           'permissions' => 'array',
            'permissions.*' => 'string|' . self::ruleInFormatted(Permission::employeePermissions()),
//            'send_info_mail' => 'nullable|boolean' // @todo Взависимости от фронтенда
//            'roles' => 'required|min:1|' . self::ruleInFormatted(),
//            'permissions' => 'required|min:1|' . self::ruleInFormatted(),
        ];
    }
}
