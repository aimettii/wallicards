<?php


namespace App\Http\Requests\API;


use InfyOm\Generator\Request\APIRequest;

class BaseAPIRequest extends APIRequest
{
    public function __construct(array $query = [], array $request = [], array $attributes = [], array $cookies = [], array $files = [], array $server = [], $content = null)
    {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
    }

    public function onlyRulesFields()
    {
        return $this->only(array_keys($this->rules()));
    }

    public static function ruleInFormatted(array $types)
    {
        return 'in:' . implode(',', $types);
    }
}