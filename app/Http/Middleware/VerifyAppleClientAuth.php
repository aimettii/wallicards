<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Hash;

class VerifyAppleClientAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $headerParts = explode(' ', $request->header('Authorization', ''));

        \Log::debug($request->header('Authorization', ''));

        if (
            $request->route('serial')
            && isset($headerParts[0]) && ($headerParts[0] === 'ApplePass' || $headerParts[0] === 'AndroidPass')
            && isset($headerParts[1])
            && Hash::check($request->route('serial'), $headerParts[1])) {
            return $next($request);
        }

        return response()->json(['error' => 'Not authorized.'], 401);
    }
}
