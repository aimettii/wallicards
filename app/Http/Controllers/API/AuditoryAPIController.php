<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateAuditoryAPIRequest;
use App\Http\Requests\API\UpdateAuditoryAPIRequest;
use App\Models\Auditory;
use App\Repositories\AuditoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

/**
 * Class AuditoryController
 * @package App\Http\Controllers\API
 */

class AuditoryAPIController extends AppBaseController
{
    /** @var  AuditoryRepository */
    public $repositoryClass = AuditoryRepository::class;

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            if (!$this->user()->company) {
                throw new BadRequestException("Для доступа к данному АПИ, пользователь должен состоять в компании"); // @todo BadRequestException
            }

            $this->repository()->setCompany($this->user()->company);

            return $next($request);
        });
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/auditories",
     *      summary="Get a listing of the Auditories.",
     *      tags={"Auditory"},
     *      description="Get all Auditories",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Auditory")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        return $this->sendResponse($this->repository()->list(), 'Auditories retrieved successfully');
    }

    /**
     * @param CreateAuditoryAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/auditories",
     *      summary="Store a newly created Auditory in storage",
     *      tags={"Auditory"},
     *      description="Store Auditory",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Auditory that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Auditory")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Auditory"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateAuditoryAPIRequest $request)
    {
        $input = $request->all();

        $auditory = $this->repository()->create($input);

        return $this->sendResponse($auditory, 'Auditory saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/auditories/{id}",
     *      summary="Display the specified Auditory",
     *      tags={"Auditory"},
     *      description="Get Auditory",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Auditory",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Auditory"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Auditory $auditory */
        $auditory = $this->repository()->find($id);

        if (empty($auditory)) {
            return $this->sendError('Auditory not found');
        }

        return $this->sendResponse($auditory, 'Auditory retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateAuditoryAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/auditories/{id}",
     *      summary="Update the specified Auditory in storage",
     *      tags={"Auditory"},
     *      description="Update Auditory",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Auditory",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Auditory that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Auditory")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Auditory"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateAuditoryAPIRequest $request)
    {
        $input = $request->all();

        $auditory = $this->repository()->find($id);

        if (empty($auditory)) {
            return $this->sendError('Auditory not found');
        }

        $auditory = $this->repository()->update($input, $id);

        return $this->sendResponse($auditory, 'Auditory updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/auditories/{id}",
     *      summary="Remove the specified Auditory from storage",
     *      tags={"Auditory"},
     *      description="Delete Auditory",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Auditory",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        if( $this->repository()->delete($id)) {
            return $this->sendSuccess('Auditory deleted successfully');
        } else {
            return $this->sendError('Auditory cannot delete', 500);
        }
    }
}
