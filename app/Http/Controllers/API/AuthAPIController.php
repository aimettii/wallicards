<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthAPIController extends AppBaseController
{
    public function loginByToken(Request $request)
    {
        $request->validate([
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'string', 'min:8']
        ]);

        $user = User::where('email', $request->email)->first();

        if (!$user || !Hash::check($request->password, $user->password)) {
            return $this->sendError('The provided credentials are incorrect', 401);
        }

        return $this->sendResponse(['token' => $user->createToken($request->email)->plainTextToken], 'Authorization successful, API token was issued');
    }

    public function loginByCookie(Request $request) {
        $email = $request->get('email');
        $password = $request->get('password');

//        $baseFieldKey = filter_var($email, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';
        $baseFieldKey = 'email';

        if (\Auth::attempt([
            $baseFieldKey => $email,
            'password' => $password
        ])) {
            return $this->sendSuccess('Authorization successful, Cookies have been set');
        }else{
            return $this->sendError('The provided credentials are incorrect', 401);
        }
    }

    public function logout()
    {
        \Auth::logout();

        return $this->sendSuccess('Cleaning the authentication information was successful');
    }
}
