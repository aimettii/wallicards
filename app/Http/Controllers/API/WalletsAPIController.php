<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Http\Controllers\Controller;
use App\Models\WalletPropertyType;
use App\Repositories\WalletRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class WalletsAPIController extends AppBaseController
{
    public $repositoryClass = WalletRepository::class;
    public $wallet;

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function($request, $next) {
            $this->wallet = $this->repository()->findByUuid($request->route('uuid'));

            if (! $this->wallet) {
                return $this->sendError('An error occurred while getting template', 422);
            }

            return $next($request);
        })->only(['backgroundImage', 'logoImage', 'pushNotificationIconImage']);
    }

    public function backgroundImage()
    {
        $walletOptions = \WalletOptions::setWalletInstance($this->wallet);

        return $walletOptions->downloadImageOption(WalletPropertyType::BACKGROUND_IMAGE);
    }

    public function logoImage()
    {
        $walletOptions = \WalletOptions::setWalletInstance($this->wallet);

        return $walletOptions->downloadImageOption(WalletPropertyType::LOGO_IMAGE);
    }

    public function pushNotificationIconImage()
    {
        $walletOptions = \WalletOptions::setWalletInstance($this->wallet);

        return $walletOptions->downloadImageOption(WalletPropertyType::PUSH_NOTIFICATION_ICON);
    }

    public function templateStyling($uuid)
    {
        $wallet = $this->repository()->findByUuid($uuid);

        if (! $wallet) {
            return $this->sendError('An error occurred while getting template', 422);
        }

        $walletOptions = \WalletOptions::setWalletInstance($this);

        $templateStyling = [
            $walletOptions->value(WalletPropertyType::LOGO_IMAGE)
                ? storage_path('app/wallet_options/' . $this->id . '/' . $walletOptions->value(WalletPropertyType::LOGO_IMAGE))
                : null
        ];

        return $this->sendResponse($templateStyling, 'Wallet template styling retrieved successfully');
    }
}
