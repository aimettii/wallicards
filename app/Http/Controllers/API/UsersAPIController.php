<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\AppBaseController;
use App\Models\Role;
use App\Models\User;
use App\Repositories\UsersRepository;
use Illuminate\Http\Request;

class UsersAPIController extends AppBaseController
{
    public $repositoryClass = UsersRepository::class;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Collection
     */
    public function index(Request $request)
    {
        return $this->sendResponse($this->repository()->all(), 'Пользователи успешно получены');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'bail|required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'roles' => 'required'
        ]);

        // Хешировать пароль
        $request->merge(['password' => bcrypt($request->get('password'))]);

        $user = $this->repository()->create($request->except('roles', 'permissions'));

        if ( $user ) {
            $this->syncPermissions($request, $user);
        } else {
            return $this->sendError('Ошибка при создании пользователя', 422);
        }

        return $this->sendResponse($user, 'Пользователь успешно создан');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = $this->repository()->find($id);

        if (empty($user)) {
            return $this->sendError('Пользователь не найден');
        }

        return $this->sendResponse($user, 'Пользователь успешно получен');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'bail|required|min:2',
            'email' => 'required|email|unique:users,email,' . $id,
            'roles' => 'required|min:1'
        ]);

        $user = $this->repository()->find($id);

        if (empty($user)) {
            return $this->sendError('Пользователь не найден');
        }

        $this->repository()->update($request->except('roles', 'permissions', 'password'), $id);

        // Проверка на изменение пароля
        if($request->get('password')) {
            $user->password = bcrypt($request->get('password'));
        }

        $this->syncPermissions($request, $user);

        $user->save();

        return $this->sendResponse($user, 'Пользователь обновлен успешно');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ( Auth::user()->id == $id ) {
            return $this->sendError('Вы не можете удалить пользователя, под которым авторизованы сейчас');
        }

        if( $this->repository()->delete($id)) {
            return $this->sendResponse(null, 'Пользователь успешно удален');
        } else {
            return $this->sendError('Не можем удалить пользователя', 500);
        }
    }

    private function syncPermissions(Request $request, User $user)
    {
        // Get the submitted roles
        $roles = $request->get('roles', []);
        $permissions = $request->get('permissions', []);

        // Get the roles
        $roles = Role::find($roles);

        // check for current role changes
        if( ! $user->hasAllRoles( $roles ) ) {
            // reset all direct permissions for user
            $user->permissions()->sync([]);
        } else {
            // handle permissions
            $user->syncPermissions($permissions);
        }

        $user->syncRoles($roles);
        return $user;
    }
}
