<?php

namespace App\Http\Controllers\API\Company;

use App\Http\Requests\API\CreateWalletCertificateAPIRequest;
use App\Http\Requests\API\UpdateWalletCertificateAPIRequest;
use App\Models\WalletCertificate;
use App\Repositories\Company\CompanyWalletCertificatesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

/**
 * Class WalletCertificateController
 * @package App\Http\Controllers\API
 */

class CompanyWalletCertificatesAPIController extends AppBaseController
{
    use CompanySetterTrait;

    /** @var  CompanyWalletCertificatesRepository */
    public $repositoryClass = CompanyWalletCertificatesRepository::class;

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function($request, $next) {
            if (! $this->user()->company) {
                throw new BadRequestException("Для доступа к данному АПИ, пользователь должен состоять в компании"); // @todo BadRequestException
            }

            $this->repository()->setCompany($this->user()->company);

            return $next($request);
        });

        $this->middleware(['permission:view_wallets'])->only('index');
        $this->middleware(['permission:add_wallets'])->only('store');
        $this->middleware(['permission:view_wallets'])->only('show');
        $this->middleware(['permission:edit_wallets'])->only('update');
        $this->middleware(['permission:delete_wallets'])->only('destroy');
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/walletCertificates",
     *      summary="Get a listing of the WalletCertificates.",
     *      tags={"WalletCertificate"},
     *      description="Get all WalletCertificates",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/WalletCertificate")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request)
    {
        return $this->sendResponse($this->repository()->all(), 'Wallet Certificates retrieved successfully');
    }

    /**
     * @param CreateWalletCertificateAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/walletCertificates",
     *      summary="Store a newly created WalletCertificate in storage",
     *      tags={"WalletCertificate"},
     *      description="Store WalletCertificate",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="WalletCertificate that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/WalletCertificate")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/WalletCertificate"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateWalletCertificateAPIRequest $request)
    {
        $input = $request->all();

        $walletCertificate = $this->repository()->create($input);

        return $this->sendResponse($walletCertificate, 'Wallet Certificate saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/walletCertificates/{id}",
     *      summary="Display the specified WalletCertificate",
     *      tags={"WalletCertificate"},
     *      description="Get WalletCertificate",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of WalletCertificate",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/WalletCertificate"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var WalletCertificate $walletCertificate */
        $walletCertificate = $this->repository()->find($id);

        if (empty($walletCertificate)) {
            return $this->sendError('Wallet Certificate not found');
        }

        return $this->sendResponse($walletCertificate, 'Wallet Certificate retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateWalletCertificateAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/walletCertificates/{id}",
     *      summary="Update the specified WalletCertificate in storage",
     *      tags={"WalletCertificate"},
     *      description="Update WalletCertificate",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of WalletCertificate",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="WalletCertificate that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/WalletCertificate")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/WalletCertificate"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateWalletCertificateAPIRequest $request)
    {
        $input = $request->all();

        $walletCertificate = $this->repository()->find($id);

        if (empty($walletCertificate)) {
            return $this->sendError('Wallet Certificate not found');
        }

        $walletCertificate = $this->repository()->update($input, $id);

        return $this->sendResponse($walletCertificate, 'WalletCertificate updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/walletCertificates/{id}",
     *      summary="Remove the specified WalletCertificate from storage",
     *      tags={"WalletCertificate"},
     *      description="Delete WalletCertificate",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of WalletCertificate",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        if( $this->repository()->delete($id)) {
            return $this->sendSuccess('Wallet Certificate deleted successfully');
        } else {
            return $this->sendError('Wallet Certificate cannot delete', 500);
        }
    }
}
