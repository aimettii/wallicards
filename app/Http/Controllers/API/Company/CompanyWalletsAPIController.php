<?php

namespace App\Http\Controllers\API\Company;

use App\Http\Requests\API\Company\CreateWalletAPIRequest;
use App\Http\Requests\API\Company\UpdateWalletAPIRequest;
use App\Models\Wallet;
use App\Repositories\Company\CompanyWalletsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

/**
 * Class WalletController
 * @package App\Http\Controllers\API
 */

class CompanyWalletsAPIController extends AppBaseController
{
    use CompanySetterTrait;

    /** @var  CompanyWalletsRepository */
    public $repositoryClass = CompanyWalletsRepository::class;

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function($request, $next) {
            if (! $this->user()->company) {
                throw new BadRequestException("Для доступа к данному АПИ, пользователь должен состоять в компании"); // @todo BadRequestException
            }

            $this->repository()->setCompany($this->user()->company);

            return $next($request);
        });

        $this->middleware(['permission:view_wallets'])->only('index');
        $this->middleware(['permission:add_wallets'])->only('store');
        $this->middleware(['permission:view_wallets'])->only('show');
        $this->middleware(['permission:edit_wallets'])->only('update');
        $this->middleware(['permission:delete_wallets'])->only('destroy');
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/wallets",
     *      summary="Get a listing of the Wallets.",
     *      tags={"Wallet"},
     *      description="Get all Wallets",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Wallet")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request)
    {
        return $this->sendResponse($this->repository()->list(), 'Wallets retrieved successfully');
    }

    /**
     * @param CreateWalletAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/wallets",
     *      summary="Store a newly created Wallet in storage",
     *      tags={"Wallet"},
     *      description="Store Wallet",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Wallet that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Wallet")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Wallet"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateWalletAPIRequest $request)
    {
        $wallet = $this->repository()->create($request->all());

        return $this->sendResponse($wallet, 'Wallet saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/wallets/{id}",
     *      summary="Display the specified Wallet",
     *      tags={"Wallet"},
     *      description="Get Wallet",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Wallet",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Wallet"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Wallet $wallet */
        $wallet = $this->repository()->find($id);

        if (empty($wallet)) {
            return $this->sendError('Wallet not found');
        }

        return $this->sendResponse($wallet, 'Wallet retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateWalletAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/wallets/{id}",
     *      summary="Update the specified Wallet in storage",
     *      tags={"Wallet"},
     *      description="Update Wallet",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Wallet",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Wallet that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Wallet")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Wallet"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateWalletAPIRequest $request)
    {
        $input = $request->all();

        $wallet = $this->repository()->find($id);

        if (empty($wallet)) {
            return $this->sendError('Wallet not found');
        }

        $wallet = $this->repository()->update($input, $id);

        return $this->sendResponse($wallet, 'Wallet updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/wallets/{id}",
     *      summary="Remove the specified Wallet from storage",
     *      tags={"Wallet"},
     *      description="Delete Wallet",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Wallet",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        if( $this->repository()->delete($id)) {
            return $this->sendSuccess('Wallet deleted successfully');
        } else {
            return $this->sendError('Wallet cannot delete', 500);
        }
    }
}
