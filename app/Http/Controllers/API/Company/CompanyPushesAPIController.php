<?php

namespace App\Http\Controllers\API\Company;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\Company\CreatePushAPIRequest;
use App\Jobs\SendPushMessage;
use App\Models\Journal;
use App\Models\User;
use App\Repositories\Company\CompanyClientsRepository;
use App\Repositories\Company\CompanyPushRepository;
use App\Repositories\Company\CompanyWalletsRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class CompanyPushesAPIController extends AppBaseController
{
    public $repositoryClass = CompanyPushRepository::class;
    public $walletsRep;
    public $clientsRep;

    public function __construct(CompanyWalletsRepository $walletsRep, CompanyClientsRepository $clientsRep)
    {
        parent::__construct();

        $this->middleware(function ($request, $next) use ($walletsRep, $clientsRep) {
            if (!$this->user()->company) {
                throw new BadRequestException("Для доступа к данному АПИ, пользователь должен состоять в компании"); // @todo BadRequestException
            }

            $this->repository()->setCompany($this->user()->company);
            $this->walletsRep = $walletsRep->setCompany($this->user()->company);
            $this->clientsRep = $clientsRep->setCompany($this->user()->company);

            return $next($request);
        });

        $this->middleware(['permission:view_pushes'])->only('index');
        $this->middleware(['permission:add_pushes'])->only('store', 'wallets', 'clients');
        $this->middleware(['permission:view_pushes'])->only('show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index()
    {
        return $this->sendResponse($this->repository()->list(), 'Pushes retrieved successfully');
    }

    public function wallets()
    {
        return $this->sendResponse($this->walletsRep->list(), 'Wallets for pushes retrieved successfully');
    }

    public function clients()
    {
        return $this->sendResponse($this->clientsRep->list(), 'Clients for pushes retrieved successfully');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePushAPIRequest $request)
    {
        $data = $request->only([
            'name',
            'message',
            'will_be_executed_at',
            'clients_ids',
        ]);

        $data['handleable_type'] = User::class;
        $data['handleable_id'] = $this->user()->id;

        $request->validate([
            'clients_ids' => function ($attribute, $value, $fail) use (&$data) {
                $clients = $this->user()->company->clients()->whereIn('clients.id', $value)->get();
                if ($clients->count() !== count($value)) {
                    $fail($attribute.' is invalid.');
                }

                $data['clients'] = $clients;
            },
        ]);

        $delayPushJob = is_null($data['will_be_executed_at']) ? 0 : Carbon::parse($data['will_be_executed_at'])->diffInSeconds(Carbon::now());
        $data['will_be_executed_at'] = is_null($data['will_be_executed_at']) ? Carbon::now() : $data['will_be_executed_at'];

        $push = $this->repository()->create($data);

        $firstClient = $push->clients->first();

        Journal::create([
            'user_id' => $this->user()->id,
            'company_id' => $firstClient->wallet->company_id,
            'wallet_id' => $firstClient->wallet_id,
            'message' => 'Создана рассылка: ' . $push->name . '. Время отправки: ' . $push->will_be_executed_at->format('H:i:s d.m.Y'),
            'type' => 'pushes',
        ]);

        SendPushMessage::dispatch($push->resource, $data['message'])->delay($delayPushJob);

        return $this->sendResponse($push, 'Push created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $push = $this->repository()->find($id);

        if (!$push) {
            return $this->sendError('Push not found', 404);
        }

        return $this->sendResponse($push, 'Push created successfully');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
