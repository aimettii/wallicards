<?php

namespace App\Http\Controllers\API\Company;

use App\Facades\SMS;
use App\Facades\WalletPass;
use App\Http\Requests\API\Company\CreateClientAPIRequest;
use App\Http\Requests\API\Company\UpdateClientAPIRequest;
use App\Http\Requests\API\CreateTransactionAPIRequest;
use App\Models\Client;
use App\Models\Journal;
use App\Repositories\Company\CompanyClientPointsHistoryRepository;
use App\Repositories\Company\CompanyClientsRepository;
use App\Repositories\Company\CompanyClientTransactionsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

/**
 * Class ClientController
 * @package App\Http\Controllers\API
 */
class CompanyClientsAPIController extends AppBaseController
{
//    use CompanySetterTrait;

    /** @var  CompanyClientsRepository */
    public $repositoryClass = CompanyClientsRepository::class;
    public $transactionsRepository;
    public $pointsHistoryRepository;

    public function __construct(Request $request, CompanyClientTransactionsRepository $transactionRepository, CompanyClientPointsHistoryRepository $pointsHistoryRepository)
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            if (!$this->user()->company) {
                throw new BadRequestException("Для доступа к данному АПИ, пользователь должен состоять в компании"); // @todo BadRequestException
            }

            $this->repository()->setCompany($this->user()->company);

            return $next($request);
        });

        $this->middleware(function ($request, $next) use ($transactionRepository, $pointsHistoryRepository) {
            $client = $this->repository()->find($request->route('id'));

            if (!$client) {
                return $this->sendError('Client not found', 404);
            }

            $this->transactionsRepository = $transactionRepository;

            $this->transactionsRepository->setClient($client->resource);

            $this->pointsHistoryRepository = $pointsHistoryRepository;

            $this->pointsHistoryRepository->setClient($client->resource);

            return $next($request);
        })->only('transactions', 'pointsHistory', 'transactionsStore');

        $this->middleware(['permission:view_clients'])->only('index', 'transactions', 'pointsHistory');
        $this->middleware(['permission:add_clients'])->only('store', 'transactionsStore');
        $this->middleware(['permission:view_clients'])->only('show');
        $this->middleware(['permission:edit_clients'])->only('update');
        $this->middleware(['permission:delete_clients'])->only('destroy');
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/clients",
     *      summary="Get a listing of the Clients.",
     *      tags={"Client"},
     *      description="Get all Clients",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Client")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request)
    {
        return $this->sendResponse($this->repository()->list(), 'Clients retrieved successfully');
    }

    public function transactions()
    {
        return $this->sendResponse($this->transactionsRepository->list(), 'Transactions retrieved successfully');
    }

    public function transactionsStore(CreateTransactionAPIRequest $request)
    {
        $input = $request->all();

        $transaction = $this->transactionsRepository->create($input);

        return $this->sendResponse($transaction, 'Transaction saved successfully');
    }

    public function pointsHistory()
    {
        return $this->sendResponse($this->pointsHistoryRepository->list(), 'Points history retrieved successfully');
    }

    /**
     * @param CreateClientAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/clients",
     *      summary="Store a newly created Client in storage",
     *      tags={"Client"},
     *      description="Store Client",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Client that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Client")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Client"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateClientAPIRequest $request)
    {
        $input = $request->all();

        $client = $this->repository()->create($input);

        if (\App::environment('production')) {
            $walletPass = WalletPass::setClient($client);
            $pkpassName = $walletPass->generatePass();
            SMS::phone($request->phone)->message('Download link: ' . env('APP_URL') . '/api/clients/pkpass/' . $pkpassName)->send();
        }

        Journal::create([
            'user_id' => $this->user()->id,
            'company_id' => $client->wallet->company_id,
            'wallet_id' => $client->wallet_id,
            'message' => 'Выдана карта: ' . $client->wallet->name . '. Серийный номер: ' . $client->serial_number,
            'type' => 'client_registered',
        ]);

        return $this->sendResponse([
            'client' => $client,
            'pkpass_url' =>
                \App::environment('production')
                    ? env('APP_URL') . '/api/clients/pkpass/' . $pkpassName
                    : null,
        ], 'Client created successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/clients/{id}",
     *      summary="Display the specified Client",
     *      tags={"Client"},
     *      description="Get Client",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Client",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Client"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Client $client */
        $client = $this->repository()->find($id);

        if (empty($client)) {
            return $this->sendError('Client not found');
        }

        return $this->sendResponse($client, 'Client retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateClientAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/clients/{id}",
     *      summary="Update the specified Client in storage",
     *      tags={"Client"},
     *      description="Update Client",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Client",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Client that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Client")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Client"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateClientAPIRequest $request)
    {
        $input = $request->all();

        $client = $this->repository()->find($id);

        if (empty($client)) {
            return $this->sendError('Client not found');
        }

        $client = $this->repository()->update($input, $id);

        return $this->sendResponse($client, 'Client updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/clients/{id}",
     *      summary="Remove the specified Client from storage",
     *      tags={"Client"},
     *      description="Delete Client",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Client",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        if ($this->repository()->delete($id)) {
            return $this->sendSuccess('Client deleted successfully');
        } else {
            return $this->sendError('Client cannot delete', 500);
        }
    }
}
