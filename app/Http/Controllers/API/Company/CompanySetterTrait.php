<?php

namespace App\Http\Controllers\API\Company;

use Symfony\Component\HttpFoundation\Exception\BadRequestException;

trait CompanySetterTrait
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware(function($request, $next) {
            if (! $this->user()->company) {
                throw new BadRequestException("Для доступа к данному АПИ, пользователь должен состоять в компании"); // @todo BadRequestException
            }

            $this->repository()->setCompany($this->user()->company);

            return $next($request);
        });
    }
}