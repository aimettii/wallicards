<?php

namespace App\Http\Controllers\API\Partner;

use App\Http\Requests\API\Partner\CreateEmployeeAPIRequest;
use App\Http\Requests\API\Partner\UpdateEmployeeAPIRequest;
use App\Mail\employeeAuthNotification;
use App\Repositories\Partner\PartnerEmployeeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Mail;
use Response;

/**
 * Class EmployeeController
 * @package App\Http\Controllers\API\Partner
 */

class PartnerEmployeeAPIController extends AppBaseController
{
    /** @var  PartnerEmployeeRepository */
    public $repositoryClass = PartnerEmployeeRepository::class;

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            $this->repository()->setPartner($this->user());
            return $next($request);
        });

        $this->middleware(['permission:view_employees'])->only('index');
        $this->middleware(['permission:add_employees'])->only('store');
        $this->middleware(['permission:view_employees'])->only('show');
        $this->middleware(['permission:edit_employees'])->only('update');
        $this->middleware(['permission:delete_employees'])->only('destroy');
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/employees",
     *      summary="Get a listing of the Employees.",
     *      tags={"Employee"},
     *      description="Get all Employees",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Employee")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request)
    {
        return $this->sendResponse($this->repository()->list(), 'Employees retrieved successfully');
    }

    /**
     * @param CreateEmployeeAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/employees",
     *      summary="Store a newly created Employee in storage",
     *      tags={"Employee"},
     *      description="Store Employee",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Employee that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Employee")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Employee"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateEmployeeAPIRequest $request)
    {
        $input = $request->all();

        $employee = $this->repository()->create($input);

        if ($request->send_info_mail) {
            Mail::to($employee->resource->resource)
                ->queue(new employeeAuthNotification($employee->resource->resource, $input['password']));
        }

        return $this->sendResponse($employee, 'Employee created successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/employees/{id}",
     *      summary="Display the specified Employee",
     *      tags={"Employee"},
     *      description="Get Employee",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Employee",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Employee"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Employee $employee */
        $employee = $this->repository()->find($id);

        if (empty($employee)) {
            return $this->sendError('Employee not found');
        }

        return $this->sendResponse($employee, 'Employee retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateEmployeeAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/employees/{id}",
     *      summary="Update the specified Employee in storage",
     *      tags={"Employee"},
     *      description="Update Employee",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Employee",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Employee that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Employee")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Employee"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateEmployeeAPIRequest $request)
    {
        $input = $request->all();

        $employee = $this->repository()->find($id);

        if (empty($employee)) {
            return $this->sendError('Employee not found');
        }

        $employee = $this->repository()->update($input, $id);

        return $this->sendResponse($employee, 'Employee updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/employees/{id}",
     *      summary="Remove the specified Employee from storage",
     *      tags={"Employee"},
     *      description="Delete Employee",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Employee",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        if( $this->repository()->delete($id)) {
            return $this->sendSuccess('Employee deleted successfully');
        } else {
            return $this->sendError('Employee cannot delete', 500);
        }
    }
}
