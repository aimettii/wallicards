<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Http\Controllers\Controller;
use App\Http\Resources\AccountResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class AccountAPIController extends AppBaseController
{
    public function __invoke(Request $request) {
        return $this->sendResponse(new AccountResource($this->user()), 'Account retrieved successfully');
    }
}