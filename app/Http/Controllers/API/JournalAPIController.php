<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateJournalAPIRequest;
use App\Http\Requests\API\UpdateJournalAPIRequest;
use App\Models\Journal;
use App\Repositories\JournalRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

/**
 * Class JournalController
 * @package App\Http\Controllers\API
 */

class JournalAPIController extends AppBaseController
{
    /** @var  JournalRepository */
    public $repositoryClass = JournalRepository::class;

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            if (!$this->user()->company) {
                throw new BadRequestException("Для доступа к данному АПИ, пользователь должен состоять в компании"); // @todo BadRequestException
            }

            $this->repository()->setCompany($this->user()->company);

            return $next($request);
        });
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/journals",
     *      summary="Get a listing of the Journals.",
     *      tags={"Journal"},
     *      description="Get all Journals",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Journal")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        return $this->sendResponse($this->repository()->list(), 'Journals retrieved successfully');
    }

//    /**
//     * @param CreateJournalAPIRequest $request
//     * @return Response
//     *
//     * @SWG\Post(
//     *      path="/journals",
//     *      summary="Store a newly created Journal in storage",
//     *      tags={"Journal"},
//     *      description="Store Journal",
//     *      produces={"application/json"},
//     *      @SWG\Parameter(
//     *          name="body",
//     *          in="body",
//     *          description="Journal that should be stored",
//     *          required=false,
//     *          @SWG\Schema(ref="#/definitions/Journal")
//     *      ),
//     *      @SWG\Response(
//     *          response=200,
//     *          description="successful operation",
//     *          @SWG\Schema(
//     *              type="object",
//     *              @SWG\Property(
//     *                  property="success",
//     *                  type="boolean"
//     *              ),
//     *              @SWG\Property(
//     *                  property="data",
//     *                  ref="#/definitions/Journal"
//     *              ),
//     *              @SWG\Property(
//     *                  property="message",
//     *                  type="string"
//     *              )
//     *          )
//     *      )
//     * )
//     */
//    public function store(CreateJournalAPIRequest $request)
//    {
//        $input = $request->all();
//
//        $journal = $this->repository()->create($input);
//
//        return $this->sendResponse($journal, 'Journal saved successfully');
//    }
//
//    /**
//     * @param int $id
//     * @return Response
//     *
//     * @SWG\Get(
//     *      path="/journals/{id}",
//     *      summary="Display the specified Journal",
//     *      tags={"Journal"},
//     *      description="Get Journal",
//     *      produces={"application/json"},
//     *      @SWG\Parameter(
//     *          name="id",
//     *          description="id of Journal",
//     *          type="integer",
//     *          required=true,
//     *          in="path"
//     *      ),
//     *      @SWG\Response(
//     *          response=200,
//     *          description="successful operation",
//     *          @SWG\Schema(
//     *              type="object",
//     *              @SWG\Property(
//     *                  property="success",
//     *                  type="boolean"
//     *              ),
//     *              @SWG\Property(
//     *                  property="data",
//     *                  ref="#/definitions/Journal"
//     *              ),
//     *              @SWG\Property(
//     *                  property="message",
//     *                  type="string"
//     *              )
//     *          )
//     *      )
//     * )
//     */
//    public function show($id)
//    {
//        /** @var Journal $journal */
//        $journal = $this->repository()->find($id);
//
//        if (empty($journal)) {
//            return $this->sendError('Journal not found');
//        }
//
//        return $this->sendResponse($journal, 'Journal retrieved successfully');
//    }
//
//    /**
//     * @param int $id
//     * @param UpdateJournalAPIRequest $request
//     * @return Response
//     *
//     * @SWG\Put(
//     *      path="/journals/{id}",
//     *      summary="Update the specified Journal in storage",
//     *      tags={"Journal"},
//     *      description="Update Journal",
//     *      produces={"application/json"},
//     *      @SWG\Parameter(
//     *          name="id",
//     *          description="id of Journal",
//     *          type="integer",
//     *          required=true,
//     *          in="path"
//     *      ),
//     *      @SWG\Parameter(
//     *          name="body",
//     *          in="body",
//     *          description="Journal that should be updated",
//     *          required=false,
//     *          @SWG\Schema(ref="#/definitions/Journal")
//     *      ),
//     *      @SWG\Response(
//     *          response=200,
//     *          description="successful operation",
//     *          @SWG\Schema(
//     *              type="object",
//     *              @SWG\Property(
//     *                  property="success",
//     *                  type="boolean"
//     *              ),
//     *              @SWG\Property(
//     *                  property="data",
//     *                  ref="#/definitions/Journal"
//     *              ),
//     *              @SWG\Property(
//     *                  property="message",
//     *                  type="string"
//     *              )
//     *          )
//     *      )
//     * )
//     */
//    public function update($id, UpdateJournalAPIRequest $request)
//    {
//        $input = $request->all();
//
//        $journal = $this->repository()->find($id);
//
//        if (empty($journal)) {
//            return $this->sendError('Journal not found');
//        }
//
//        $journal = $this->repository()->update($input, $id);
//
//        return $this->sendResponse($journal, 'Journal updated successfully');
//    }
//
//    /**
//     * @param int $id
//     * @return Response
//     *
//     * @SWG\Delete(
//     *      path="/journals/{id}",
//     *      summary="Remove the specified Journal from storage",
//     *      tags={"Journal"},
//     *      description="Delete Journal",
//     *      produces={"application/json"},
//     *      @SWG\Parameter(
//     *          name="id",
//     *          description="id of Journal",
//     *          type="integer",
//     *          required=true,
//     *          in="path"
//     *      ),
//     *      @SWG\Response(
//     *          response=200,
//     *          description="successful operation",
//     *          @SWG\Schema(
//     *              type="object",
//     *              @SWG\Property(
//     *                  property="success",
//     *                  type="boolean"
//     *              ),
//     *              @SWG\Property(
//     *                  property="data",
//     *                  type="string"
//     *              ),
//     *              @SWG\Property(
//     *                  property="message",
//     *                  type="string"
//     *              )
//     *          )
//     *      )
//     * )
//     */
//    public function destroy($id)
//    {
//        if( $this->repository()->delete($id)) {
//            return $this->sendSuccess('Journal deleted successfully');
//        } else {
//            return $this->sendError('Journal cannot delete', 500);
//        }
//    }
}
