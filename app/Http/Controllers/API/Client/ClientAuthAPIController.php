<?php

namespace App\Http\Controllers\API\Client;

use App\Exceptions\ErrorHandlers\ClientAuthHandler;
use App\Facades\SMS;
use App\Facades\WalletPass;
use App\Http\Requests\API\Client\RegisterClientAPIRequest;
use App\Models\Client;
use App\Models\Journal;
use App\Models\WalletPropertyType;
use App\Repositories\ClientRepository;
use App\Http\Controllers\AppBaseController;
use App\Repositories\WalletRepository;
use App\Services\ErrorHandler;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Response;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

/**
 * Class ClientController
 * @package App\Http\Controllers\API
 */
class ClientAuthAPIController extends AppBaseController
{
    /** @var  ClientRepository */
    public $repositoryClass = ClientRepository::class;
    public $errorHandlerClass = ClientAuthHandler::class;
    public $walletRepository;
    public $platform;
    public $wallet;

    public function __construct(Request $request, WalletRepository $walletRepository)
    {
        parent::__construct();

        $this->walletRepository = $walletRepository;

//        if (! $request->userAgent()) {
//            throw new BadRequestException('User agent not available'); // @todo BadRequestException
//        }

        $userAgent = $request->userAgent();

        if (stripos($userAgent, 'android') !== false || stripos($userAgent, 'WalletPasses') !== false) {
            $this->platform = Client::PLATFORM_ANDROID;
        } else {
//            $this->platform = Client::PLATFORM_ANDROID; // @todo for TESTS
            $this->platform = Client::PLATFORM_IOS;
//            throw new BadRequestException('Bad user agent platform'); // @todo BadRequestException
        }

        $this->middleware(function($request, $next) {
            $this->wallet = $this->walletRepository->findByUuid($request->route('uuid'));

            if (! $this->wallet) {
                return $this->sendError('An error occurred while getting template', 422);
            }

            return $next($request);
        })->only(['templateDesign', 'logoImage', 'sendSMS', 'register']);

//        $this->middleware('start_session')->only('sendSMS', 'verifyDevice');
    }

    public function sendSMS($uuid, $phone)
    {
        $validate = Validator::make([
            'phone' => $phone,
        ], [
            'phone'     => 'required|digits:11|regex:/7[\d]{10,}/',
        ]);

        if ($validate->fails()) {
            return $this->sendError($validate->errors()->first(), 422);
        }

        $verifyCode = mt_rand(1000, 9999);

        $redisKey = 'client_register_sms_verification_' . $phone . '_' . $uuid;

        $verification = Redis::get($redisKey);

        $sentCount = 0;
        $attemptCount = 0;

        if ($verification) {
            $verification = json_decode($verification);

            $sentCount = $verification->sent_count;

            $diff = Carbon::parse($verification->sent_at)->diff(Carbon::now());
            $diffSeconds = date_create('@0')->add($diff)->getTimestamp();

            // @todo Добавить условие еще на проверку попыток ввода кода, а не просто отправок смс
            if ($verification->sent_count >= 3) {
                if ($diffSeconds < 360) { // < 5 минут
                    return $this->sendError(ClientAuthHandler::TOO_MANY_SMS_SENT);
                } else {
                    $sentCount = 0;
                }
            }

            if ($verification->attempts_count >= 3) {
                $diff = Carbon::parse($verification->last_attempt_at)->diff(Carbon::now());
                $diffLastAttemptSeconds = date_create('@0')->add($diff)->getTimestamp();

                if ($diffLastAttemptSeconds < 180) {
                    return $this->sendError(ClientAuthHandler::TOO_MANY_VERIFICATION_ATTEMPTS);
                } else {
                    $attemptCount = 0;
                }
            }

            if ($diffSeconds < 59) {
                return $this->sendError(ClientAuthHandler::TOO_FREQUENT_REQUESTS);
            }
        }

        try {
            SMS::phone($phone)->message('Verification code: ' . $verifyCode)->send();

            $sentCount++;
            
            Redis::set($redisKey, json_encode([
                'sent_at' => Carbon::now(),
                'sent_count' => $sentCount,
                'code' => $verifyCode,
                'verified' => false,
                'attempts_count' => $attemptCount,
                'last_attempt_at' => null,
                'uuid' => $uuid
            ]));
        } catch (\Exception $e) {
            Redis::del($redisKey);

            return $this->sendError(ClientAuthHandler::ERROR_SENDING_SMS);
        }

        return $this->sendSuccess('Verification code was sent successfully');
    }

    public function verifyDevice($uuid, $phone, $code)
    {
        $validate = Validator::make([
            'phone' => $phone,
        ], [
            'phone'     => 'required|digits:11|regex:/7[\d]{10,}/',
        ]);

        if ($validate->fails()) {
            return $this->sendError($validate->errors()->first(), 422);
        }

        $redisKey = 'client_register_sms_verification_' . $phone . '_' . $uuid;

        $verification = Redis::get($redisKey);

        if ($verification) {
            $verification = json_decode($verification);

            if ($verification->verified) {
                return $this->sendSuccess('Already verified');
            }

            $diff = Carbon::parse($verification->last_attempt_at)->diff(Carbon::now());
            $diffSeconds = date_create('@0')->add($diff)->getTimestamp();

            if ($verification->attempts_count >= 3) {
                if ($diffSeconds < 180) {
                    return $this->sendError(ClientAuthHandler::TOO_MANY_VERIFICATION_ATTEMPTS);
                } else {
                    $verification->attempts_count = 0;
                }
            }

            $verification->attempts_count++;
            $verification->last_attempt_at = Carbon::now();
            Redis::set($redisKey, json_encode($verification));

            if ($verification->code !== (int) $code) {
                return $this->sendError(ClientAuthHandler::INCORRECT_CODE);
            } else {
                $verification->verified = true;
                Redis::set($redisKey, json_encode($verification));

                return $this->sendSuccess('Verified was successfully');
            }
        }

        return $this->sendError(ClientAuthHandler::NOT_FOUND_VERIFICATION_SMS_SENT);
    }

    public function templateDesign($uuid, Request $request)
    {
        $walletOptions = \WalletOptions::setWalletInstance($this->wallet);

        $walletTemplate = [
            'logo_image' => $this->wallet->logo_image_url,
            'background_colors' => $walletOptions->value(WalletPropertyType::BACKGROUND_COLORS),
            'value_colors' => $walletOptions->value(WalletPropertyType::VALUE_COLORS),
            'wallet_type' => $this->wallet->type_name,
            'logo_text' => $walletOptions->value(WalletPropertyType::LOGO_TEXT),
        ];

        return $this->sendResponse($walletTemplate, 'Wallet template retrieved successfully');
    }

    public function getPkpassFile($hash)
    {
        $client = Client::where('pkpass_file', $hash)->first();

        if (! $client) {
            return $this->sendError('An error occurred while pkpass downloading', 422);
        }

        return $client->downloadPassFile($client->serial_number);
    }

    public function register($uuid, RegisterClientAPIRequest $request)
    {
        $responseJson = null;

        if (\App::environment('local')) {
            $response = \Http::post(env('PRODUCTION_URL') . '/api/wallets/'. $uuid .'/register', $request->merge([
                'secret-uuid-verification-1' => '2bacdabb-6c27-420f-8d69-a51c50adec4c',
                'secret-uuid-verification-2' => '0988ba47-a9f1-49dc-befc-c3ea23dc363f',
            ])->all());

            $responseJson = $response->json();

            if (isset($responseJson['success']) && isset($responseJson['data'])) {

            } else {
                return $this->sendError($responseJson ? array_merge($responseJson, ['errorProduction' => true]) : [
                    'errorProduction' => true,
                    'errMessage' => 'Check production errors'
                ], 422);
            }
        }

        $redisKey = 'client_register_sms_verification_' . $request->phone . '_' . $uuid;

        if (
            ! (
                $request->input('secret-uuid-verification-1') === '2bacdabb-6c27-420f-8d69-a51c50adec4c' &&
                $request->input('secret-uuid-verification-2') === '0988ba47-a9f1-49dc-befc-c3ea23dc363f'
            )
        ) {
            $verification = Redis::get($redisKey);

            if ($verification) {
                $verification = json_decode($verification);

                if (! $verification->verified) {
                    return $this->sendError(ClientAuthHandler::NOT_VERIFIED);
                }
            } else {
                return $this->sendError(ClientAuthHandler::NOT_FOUND_VERIFICATION_SMS_SENT);
            }
        }

        Redis::del($redisKey);

        $this->repository()->setWallet($this->wallet);

        $client = $this->repository()->create(
            array_merge(
                $request->onlyRulesFields()
//                ['platform' => $this->platform]
            )
        );

        if (\App::environment('production')) {
            $pkpassName = WalletPass::setClient($client)->generatePass();
        } else if ($responseJson) {
            $walletPass = WalletPass::setClient($client);

            if (file_put_contents(
                $walletPass->pkpassFile(),
                fopen($responseJson['data']['pkpass_url'], 'r')
            ) && file_exists($walletPass->pkpassFile())) {
                $pkpassName = $client->updatePassFile($walletPass->passName);
            } else {
                throw new FileException('Не можем сохранить pkpass файл на локале');
            }
        } else {
            throw new FileException('Неизвестная ошибка на локале');
        }

        Journal::create([
            'user_id' => null,
            'company_id' => $client->wallet->company_id,
            'wallet_id' => $client->wallet_id,
            'message' => 'Выдана карта: ' . $client->wallet->name . '. Серийный номер: ' . $client->serial_number,
            'type' => 'client_registered',
        ]);

        return $this->sendResponse([
            'client' => $client,
            'pkpass_url' => \App::environment('production') ? env('APP_URL') . '/api/clients/pkpass/' . $pkpassName : $responseJson['data']['pkpass_url'],
        ], 'Client created successfully');
    }

    public function registerDeviceWebhook(Request $request, $deviceId, $passType, $serial)
    {
        $client = $this->repository()->findBySerial($serial);

        if (empty($client)) {
            return $this->sendError(ClientAuthHandler::CLIENT_NOT_FOUND);
        }

        if ($client->status_on_device === Client::STATUS_DEVICE_ACTIVE) {
            return $this->sendSuccess('Client already was registered', 200);
        }

        $client = $this->repository()->update([
            'device_id' => $deviceId,
            'push_token' => $request->pushToken,
            'platform' => $this->platform,
            'status_on_device' => Client::STATUS_DEVICE_ACTIVE
        ], $client->id);

        $walletOptions = \WalletOptions::setWalletInstance($client->wallet);
        $changeMessage = $walletOptions->option(WalletPropertyType::CHANGE_MESSAGE);

        if ($changeMessage) {
            $walletPass = WalletPass::setClient($client);
            $walletPass->sendPushMessage($changeMessage->value);
        }

        return $this->sendSuccess('Client registered successfully', 201);
    }

    public function getSerialNumbersWebhook(Request $request, $deviceId, $passType)
    {
        $clients = Client::where('device_id', $deviceId);

        if ($request->passesUpdatedSince) {
            $clients->where('updated_at', '>', Carbon::rawCreateFromFormat('d/m/Y H:i:s', $request->passesUpdatedSince));
        }

        $clients = $clients->get();

        if (empty($clients)) {
            return $this->sendSuccess('Not found passes', 204);
        }

        return response()
            ->json(['serialNumbers' => $clients->map(function($item) {
                return (string) $item->serial_number;
            })->all(), 'lastUpdated' => date("d/m/Y H:i:s")])
            ->header('Last-Modified', gmdate('D, d M Y H:i:s T'));
    }

    public function getLatestVersionOfPassWebhook(Request $request, $passType, $serial)
    {
        $client = $this->repository()->findBySerial($serial);

        if (empty($client)) {
            return $this->sendError(ClientAuthHandler::CLIENT_NOT_FOUND);
        }

        $ifModifiedSince = $request->header('If-Modified-Since');

        if ($ifModifiedSince && Carbon::parse($client->updated_at)->lessThan(Carbon::rawCreateFromFormat('D, d M Y H:i:s T', $ifModifiedSince))) {
            return $this->sendSuccess('Pass has not changed', 304);
        }

        return $client->downloadPassFile($client->serial_number, [
            'Last-Modified' => gmdate('D, d M Y H:i:s T')
        ]);
    }

    public function deleteDeviceWebhook(Request $request, $deviceId, $passType, $serial)
    {
        $client = $this->repository()->findBySerial($serial);

        if (empty($client)) {
            return $this->sendError(ClientAuthHandler::CLIENT_NOT_FOUND);
        }

        if ($client->device_id !== $deviceId) {
            return response()->json(['error' => 'Not authorized.'], 401);
        }

//        if ($client->status_on_device !== Client::STATUS_DEVICE_ACTIVE) {
//            return $this->sendError('Client not registered device', 403);
//        }

        $this->repository()->update([
            'status_on_device' => Client::STATUS_DEVICE_DELETED
        ], $client->id);

        Journal::create([
            'user_id' => null,
            'company_id' => $client->wallet->company_id,
            'wallet_id' => $client->wallet_id,
            'message' => 'Удалена карта: ' . $client->wallet->name . '. Серийный номер: ' . $client->serial_number,
            'type' => 'client_deleted',
        ]);

        return $this->sendSuccess('Cancel client authorization successfully', 200);
    }

    public function logWebhook(Request $request)
    {
        Log::debug($request->logs);

        return $this->sendSuccess('');
    }
}
