<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Pagination\LengthAwarePaginator;
use InfyOm\Generator\Utils\ResponseUtil;
use Illuminate\Support\Facades\Auth;
use Response;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

/**
 * @SWG\Swagger(
 *   basePath="/api/v1",
 *   @SWG\Info(
 *     title="Laravel Generator APIs",
 *     version="1.0.0",
 *   )
 * )
 * This class should be parent class for other API controllers
 * Class AppBaseController
 */
class AppBaseController extends Controller
{
    private $repository;
    private $errorHandler;
    private $user;

    public function __construct()
    {
        if (isset($this->repositoryClass)) {

            if (!class_exists($this->repositoryClass)) {
                throw new BadRequestException("Repository class not found"); // @todo BadRequestException
            }

            $this->repository = new $this->repositoryClass(app());
        }

        if (isset($this->errorHandlerClass)) {

            if (!class_exists($this->errorHandlerClass)) {
                throw new BadRequestException("ErorHandler class not found"); // @todo BadRequestException
            }

            $this->errorHandler = new $this->errorHandlerClass(app());
        }


        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();

            return $next($request);
        });
    }

    /**
     * @return BaseRepository
     */
    public function repository()
    {
        if (!$this->repository || !$this->repository instanceof BaseRepository) {
            throw new BadRequestException('Что бы использовать паттерн репозиторий, сначала укажите namespace репозитория, 
            наследующегося от App\Repositories\BaseRepository в repositoryClass свойстве вашего контроллера');
        }

        return $this->repository;
    }

    /**
     * @return User
     */
    public function user()
    {
        if (!$this->user) {
            throw new BadRequestException("Авторизация Sanctum обязательна для данного типа API"); // @todo BadRequestException
        }

        return $this->user;
    }

    public function sendResponse($result, $message)
    {
        $arrResponse = ResponseUtil::makeResponse($message, $result);

        if ($result instanceof ResourceCollection && $result->resource instanceof LengthAwarePaginator) {
            $arrResponse = array_merge($arrResponse, (array) $result->toResponse(new Request())->getData());
        }

        return Response::json($arrResponse);
    }

    public function sendError($error, $code = null, int $typeCode = null, string $description = null)
    {
        if ($this->errorHandler && is_int($error) && ! is_null($this->errorHandler->message($error))) {
            $code = $code ?: $this->errorHandler->code($error);
            $description = $description ?: $this->errorHandler->description($error);
            $typeCode = $typeCode ?: $error;
            $error = $this->errorHandler->message($error);
        } else {
            $code = 404;
        }

        $arrResponse = ResponseUtil::makeError($error);

        if ($typeCode) {
            $arrResponse['code'] = $typeCode;
        }

        if ($description) {
            $arrResponse['description'] = $description;
        }

        return Response::json($arrResponse, $code);
    }

    public function sendSuccess($message,  $code = 200)
    {
        return Response::json([
            'success' => true,
            'message' => $message
        ], $code);
    }
}
