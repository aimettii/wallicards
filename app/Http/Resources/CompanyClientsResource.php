<?php

namespace App\Http\Resources;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class CompanyClientsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->when($this->id, $this->id),
            'name' => $this->when($this->name, $this->name),
            'phone' => $this->when($this->phone, $this->phone ? $this->phone->phone_number : null),
            'serial_number' => $this->when($this->serial_number, $this->serial_number),
            'gender' => $this->when($this->gender, $this->gender),
            'date_of_birth' => $this->when($this->date_of_birth, $this->date_of_birth),
            'age' => $this->when($this->date_of_birth, Carbon::parse($this->date_of_birth)->age),
            'platform' => $this->when($this->platform, $this->platform),
            'status_on_device' => $this->when($this->status_on_device, $this->status_on_device),
            'balance' => $this->when(is_int($this->balance), $this->balance),
            'points_spent' => $this->when(is_int($this->points_spent), $this->points_spent),
            'amount_purchase' => $this->when(is_int($this->amount_purchase), $this->amount_purchase),
            'last_transaction_amount_purchase' => $this->when(is_int($this->last_transaction_amount_purchase), $this->last_transaction_amount_purchase),
            'last_visit' => $this->when($this->lastVisit, $this->lastVisit),
//            'transactions' => $this->when($this->transactions, TransactionsResource::collection($this->transactions->load('options'))),
//            'points_history' => $this->when($this->points_history, $this->points_history->map(function($item) use ($request) {
//                $formatted = collect($item)->only([
//                    'type',
//                    'note',
//                    'points',
//                    'created_at',
//                    'updated_at'
//                ])->toArray();
//
//                $transaction = $item['transaction'];
//
//                $formatted['employee_name'] = $transaction->handleable_type === User::class && $transaction->wasHandled ? $transaction->wasHandled->name : null;
//
//                return $formatted;
//            })),
            'wallet_name' => $this->when($this->wallet, $this->wallet->name),
            'status' => $this->when($this->status, $this->status),
            'favorite' => $this->when(isset($this->favorite), $this->favorite),
            'created_at' => $this->when($this->created_at, $this->created_at),
            'updated_at' => $this->when($this->updated_at, $this->updated_at),
        ];
    }
}
