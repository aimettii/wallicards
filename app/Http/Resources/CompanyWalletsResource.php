<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CompanyWalletsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->when($this->id, $this->id),
            'uuid' => $this->when($this->uuid, $this->uuid),
            'name' => $this->when($this->name, $this->name),
            'wallet_type_id' => $this->when($this->wallet_type_id, $this->wallet_type_id),
            'type_name' => $this->when($this->type_name, $this->type_name),
            'certificate_id' => $this->when($this->certificate_id, $this->certificate_id),
            'company_id' => $this->when($this->company_id, $this->company_id),
            'logo_image' => $this->when($this->logo_image_url, $this->logo_image_url),
            'background_image' => $this->when($this->background_image_url, $this->background_image_url),
            'options' => $this->when($this->options, \WalletOptions::setWalletInstance($this->resource)->optionInstanceToRequestParams()),
            'created_at' => $this->when($this->created_at, $this->created_at),
            'updated_at' => $this->when($this->updated_at, $this->updated_at),
        ];
    }
}
