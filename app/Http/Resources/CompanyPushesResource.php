<?php

namespace App\Http\Resources;

use App\Models\Push;
use Illuminate\Http\Resources\Json\JsonResource;

class CompanyPushesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request) + [
                'employee_name' => $this->when($this->handleable_type === 'App\Models\User' && $this->wasHandled,
                    $this->wasHandled ? $this->wasHandled->name : null),
                'wallet_name' => $this->wallet ? $this->wallet->name : null,
                'clients' => $this->clients->map(function($client) {
                    return [
                        'status' => $client->pivot->status,
                        'name' => $client->name,
                        'serial_number' => $client->serial_number
                    ];
                })->all(),
                'sent_count' => $this->clients->where('pivot.status', Push::STATUS_PERFORMED)->count(),
                'status' =>
                    $this->clients->where('pivot.status', Push::STATUS_PROCESS)->first()
                        ? Push::STATUS_PROCESS
                        : (
                    $this->clients->where('pivot.status', Push::STATUS_FAILED)->first()
                        ? Push::STATUS_FAILED
                        : Push::STATUS_PERFORMED
                    ),
            ];
    }
}
