<?php

namespace App\Http\Resources;

use App\Models\TransactionOptionType;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class CompanyClientPointsHistoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $formatted = collect($this->resource)->only([
            'type',
            'note',
            'points',
            'created_at',
            'updated_at'
        ])->toArray();

        $transaction = $this->resource['transaction'];

        $formatted['employee_name'] = $transaction->handleable_type === User::class && $transaction->wasHandled ? $transaction->wasHandled->name : null;

        return $formatted;
    }
}
