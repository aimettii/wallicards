<?php

namespace App\Http\Resources;

use App\Models\TransactionOptionType;
use Illuminate\Http\Resources\Json\ResourceCollection;

class CompanyClientPointsHistoryCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return CompanyClientPointsHistoryResource::collection($this->collection);
    }
}
