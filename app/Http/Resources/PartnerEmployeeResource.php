<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PartnerEmployeeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->when('id', $this->id),
            'email' => $this->when('email', $this->email),
            'name' => $this->when($this->name, $this->name),
            'phone' => $this->when('phone', $this->phone->phone_number),
            'note' => $this->when('note', $this->note),
            'roles' => $this->roles ? $this->roles->map->name : [],
            'permissions' => $this->getAllPermissions()->map->name,
        ];
    }
}
