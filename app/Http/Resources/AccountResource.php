<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AccountResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->when('id', $this->id),
            'email' => $this->when('email', $this->email),
            'name' => $this->when('name', $this->name),
            'phone' => $this->when('phone', $this->phone->phone_number),
            'roles' => $this->roles ? $this->roles->map->name : [],
            'permissions' => $this->getAllPermissions()->map->name,
            'company_id' => $this->when('company', $this->company->id),
            'created_at' => $this->when('created_at', $this->created_at),
            'updated_at' => $this->when('updated_at', $this->updated_at),
        ];
    }
}
