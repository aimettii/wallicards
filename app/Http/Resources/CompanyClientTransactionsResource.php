<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CompanyClientTransactionsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request) + [
                'employee_name' => $this->when($this->handleable_type === 'App\Models\User' && $this->wasHandled,
                    $this->wasHandled ? $this->wasHandled->name : null),
                'options' => $this->when($this->options, $this->options)
            ];
    }
}
