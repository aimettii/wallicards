<?php

namespace App\Repositories;

use App\Models\Push;
use App\Repositories\BaseRepository;

/**
 * Class PushRepository
 * @package App\Repositories
 * @version July 22, 2020, 5:51 pm UTC
*/

class PushRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $allowedFilters = [
        'handleable_type',
        'handleable_id',
        'message',
        'will_be_executed_at'
    ];

    /**
     * @var array
     */
    protected $allowedFields = [
        'handleable_type',
        'handleable_id',
        'message',
        'will_be_executed_at'
    ];

    /**
     * @var array
     */
    protected $allowedSorts = [
        'handleable_type',
        'handleable_id',
        'message',
        'will_be_executed_at'
    ];

    /**
     * @var array
     */
    protected $allowedIncludes = [];

    /**
     * @var array
     */
    protected $allowedAppends = [];

    /**
     * Return allowed filters
     *
     * @return array
     */
    public function getAllowedFilters()
    {
        return $this->allowedFilters;
    }

    /**
     * Return allowed fields
     *
     * @return array
     */
    public function getAllowedFields()
    {
        return $this->allowedFields;
    }

    /**
     * Return allowed sorts
     *
     * @return array
     */
    public function getAllowedSorts()
    {
        return $this->allowedSorts;
    }

    /**
     * Return allowed includes
     *
     * @return array
     */
    public function getAllowedIncludes()
    {
        return $this->allowedIncludes;
    }

    /**
     * Return allowed appends
     *
     * @return array
     */
    public function getAllowedAppends()
    {
        return $this->allowedAppends;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Push::class;
    }

    public function create($input)
    {
        $modelInput = collect($input)->only([
            'name',
            'message',
            'will_be_executed_at',
            'handleable_type',
            'handleable_id'
        ])->toArray();

        $model = $this->model->newInstance($modelInput);

        $model->save();

        $model->clients()->saveMany($input['clients'], collect($input['clients'])->map(function ($item) {
            return [
                'status' => Push::STATUS_PROCESS
            ];
        })->all());

        return $this->modelWrapper($model);
    }
}
