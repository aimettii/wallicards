<?php

namespace App\Repositories;

use App\Models\Auditory;
use App\Models\Wallet;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Builder;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

/**
 * Class AuditoryRepository
 * @package App\Repositories
 * @version August 5, 2020, 12:17 pm UTC
*/

class AuditoryRepository extends BaseRepository
{
    use \CompanySetterTrait;

    /**
     * @var array
     */
    protected $allowedFilters = [
        'filter'
    ];

    /**
     * @var array
     */
    protected $allowedFields = [
        'filter'
    ];

    /**
     * @var array
     */
    protected $allowedSorts = [
        'filter'
    ];

    /**
     * @var array
     */
    protected $allowedIncludes = [];

    /**
     * @var array
     */
    protected $allowedAppends = [];

    /**
     * Return allowed filters
     *
     * @return array
     */
    public function getAllowedFilters()
    {
        return $this->allowedFilters;
    }

    /**
     * Return allowed fields
     *
     * @return array
     */
    public function getAllowedFields()
    {
        return $this->allowedFields;
    }

    /**
     * Return allowed sorts
     *
     * @return array
     */
    public function getAllowedSorts()
    {
        return $this->allowedSorts;
    }

    /**
     * Return allowed includes
     *
     * @return array
     */
    public function getAllowedIncludes()
    {
        return $this->allowedIncludes;
    }

    /**
     * Return allowed appends
     *
     * @return array
     */
    public function getAllowedAppends()
    {
        return $this->allowedAppends;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Auditory::class;
    }

    /**
     * @return Builder
     * @throws \Exception
     */
    public function newQuery(): Builder
    {
        $query = parent::newQuery();

        if (! $this->company) {
            throw new BadRequestException("Необходимо указать компанию"); // @todo BadRequestException
        }

        return $query->where('company_id', $this->company->id);
    }

    public function create($input)
    {
        $model = parent::create(array_merge($input, [
            'company_id' => $this->company->id
        ])
        );

        return $model;
    }
}
