<?php

namespace App\Repositories;

use App\Services\RequestListParameters;
use Illuminate\Container\Container as Application;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Spatie\QueryBuilder\QueryBuilder;


abstract class BaseRepository
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * @var Application
     */
    protected $app;

    /**
     * @var RequestListParameters
     */
    protected $listParameters;

    /**
     * @param Application $app
     *
     * @throws \Exception
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->makeModel();
        $this->listParameters = new RequestListParameters(request()->query());
    }

    /**
     * Get allowed filters array
     *
     * @return array
     */
    abstract public function getAllowedFilters();

    /**
     * Get allowed fields array
     *
     * @return array
     */
    abstract public function getAllowedFields();

    /**
     * Get allowed sorts array
     *
     * @return array
     */
    abstract public function getAllowedSorts();

    /**
     * Get allowed includes array
     *
     * @return array
     */
    abstract public function getAllowedIncludes();

    /**
     * Get allowed appends array
     *
     * @return array
     */
    abstract public function getAllowedAppends();

    /**
     * Configure the Model
     *
     * @return string
     */
    abstract public function model();

    /**
     * Make Model instance
     *
     * @throws \Exception
     *
     * @return Model
     */
    public function makeModel()
    {
        $model = $this->app->make($this->model());

        if (!$model instanceof Model) {
            throw new \Exception("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");
        }

        return $this->model = $model;
    }
    
    public function newQuery(): Builder
    {
        return $this->model->newQuery();
    }

    /**
     * Paginate records for scaffold.
     *
     * @param array $search
     * @param int $perPage
     * @param null $only
     * @param array $columns
     * @param null $sortSettings
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     * @throws \Illuminate\Validation\ValidationException
     */
    public function paginate()
    {
        $query = $this->allQuery();

        return $query->paginate($this->listParameters->pagination('per_page'))->appends(request()->query());
    }

    /**
     * Build a query for retrieving all records.
     *
     * @param array $search
     * @param int|null $skip
     * @param int|null $limit
     * @param null $except
     * @param null $random
     * @param null $only
     * @param null $sortSettings
     * @return \Illuminate\Database\Eloquent\Builder
     * @throws \Illuminate\Validation\ValidationException
     */
    public function allQuery()
    {
        $query = QueryBuilder::for($this->newQuery())
            ->allowedFields($this->getAllowedFields())
            ->allowedFilters($this->getAllowedFilters())
            ->allowedSorts($this->getAllowedSorts())
            ->allowedIncludes($this->getAllowedIncludes())
            ->allowedAppends($this->getAllowedAppends());

//        if (! $this->listParameters->hasPagination() ) {
//            $skip = $this->listParameters->getOptions('skip');
//            $limit = $this->listParameters->getOptions('limit');
//
//            if (! is_null($skip)) {
//                $query->skip($skip);
//            }
//
//            if (! is_null($limit)) {
//                $query->limit($limit);
//            }
//        }

        $except = $this->listParameters->getOptions('except');

        if (! is_null($except)) {
            if (! is_array($except)) {
                $except = [$except];
            }

            $query->whereNotIn('id' , $except);
        }

//        $only = $this->listParameters->getOptions('only');
//
//        if (! is_null($only)) {
//            if (! is_array($only)) {
//                $only = [$only];
//            }
//
//            $query->whereIn('id' , $only);
//        }

        $random = $this->listParameters->getOptions('random');

        if (!is_null($random)) {
            $query->inRandomOrder();
        }

        return $query;
    }

    /**
     * Retrieve all records with given filter criteria
     *
     * @param null $sortSettings
     * @return \Illuminate\Database\Eloquent\Collection|Model
     * @throws \Illuminate\Validation\ValidationException
     */
    public function all($query = null)
    {
        $query = $query ? $query : $this->allQuery();

        $resource = $this->resource();
        $resourceCollection = $this->resourceCollection();

        $items = $query instanceof LengthAwarePaginator ? $query : $query->get();

        if ($items instanceof LengthAwarePaginator && $resourceCollection) {
            return new $resourceCollection($items);
        }

        if ($items instanceof Collection && $resource) {
            return $resource::collection($items);
        }

        return $items;
    }

    /**
     * @return Collection|Model
     * @throws \Illuminate\Validation\ValidationException
     */
    public function list()
    {
        $query = $this->listParameters->hasPagination() ? $this->paginate() : $this->allQuery();

        return $this->all($query);
    }

    /**
     * Create model record
     *
     * @param array $input
     *
     * @return Model
     */
    public function create($input)
    {
        $model = $this->model->newInstance($input);

        $model->save();

        return $this->modelWrapper($model);
    }

    /**
     * Find model record for given id
     *
     * @param int $id
     * @param array $columns
     *
     * @return Model|null
     */
    public function find($id)
    {
        $query = QueryBuilder::for($this->newQuery())
            ->allowedFields($this->getAllowedFields())
            ->allowedIncludes($this->getAllowedIncludes())
            ->allowedAppends($this->getAllowedAppends());

        $item = $query->find($id);

        return !is_null($item) ? $this->modelWrapper($item) : null;
    }

    /**
     * Update model record for given id
     *
     * @param array $input
     * @param int $id
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|Model
     */
    public function update($input, $id)
    {
        $query = $this->newQuery();

        $model = $query->findOrFail($id);

        $model->fill($input);

        $model->save();

        return $this->modelWrapper($model);
    }

    /**
     * @param int $id
     *
     * @throws \Exception
     *
     * @return bool|mixed|null
     */
    public function delete($id)
    {
        $query = $this->newQuery();

        $model = $query->findOrFail($id);

        return $model->delete();
    }

    public function modelWrapper($model)
    {
        $resource = $this->resource();
        return $resource ? new $resource($model) : $model;
    }

    public function resource()
    {
        return null;
    }

    public function resourceCollection()
    {
        return null;
    }
}
