<?php

namespace App\Repositories;

use App\Models\Journal;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Builder;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

/**
 * Class JournalRepository
 * @package App\Repositories
 * @version August 5, 2020, 1:05 pm UTC
*/

class JournalRepository extends BaseRepository
{
    use \CompanySetterTrait;

    /**
     * @var array
     */
    protected $allowedFilters = [
        'user_id',
        'company_id',
        'message'
    ];

    /**
     * @var array
     */
    protected $allowedFields = [
        'user_id',
        'company_id',
        'message'
    ];

    /**
     * @var array
     */
    protected $allowedSorts = [
        'user_id',
        'company_id',
        'message'
    ];

    /**
     * @var array
     */
    protected $allowedIncludes = [];

    /**
     * @var array
     */
    protected $allowedAppends = [];

    /**
     * Return allowed filters
     *
     * @return array
     */
    public function getAllowedFilters()
    {
        return $this->allowedFilters;
    }

    /**
     * Return allowed fields
     *
     * @return array
     */
    public function getAllowedFields()
    {
        return $this->allowedFields;
    }

    /**
     * Return allowed sorts
     *
     * @return array
     */
    public function getAllowedSorts()
    {
        return $this->allowedSorts;
    }

    /**
     * Return allowed includes
     *
     * @return array
     */
    public function getAllowedIncludes()
    {
        return $this->allowedIncludes;
    }

    /**
     * Return allowed appends
     *
     * @return array
     */
    public function getAllowedAppends()
    {
        return $this->allowedAppends;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Journal::class;
    }

    /**
     * @return Builder
     * @throws \Exception
     */
    public function newQuery(): Builder
    {
        $query = parent::newQuery();

        if (! $this->company) {
            throw new BadRequestException("Необходимо указать компанию"); // @todo BadRequestException
        }

        return $query->where('company_id', $this->company->id);
    }
}
