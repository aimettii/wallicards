<?php

namespace App\Repositories\Partner;

use App\Http\Resources\PartnerEmployeeCollection;
use App\Http\Resources\PartnerEmployeeResource;
use App\Models\Role;
use App\Models\User;
use App\Repositories\EmployeeRepository;
use Illuminate\Database\Eloquent\Builder;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class PartnerEmployeeRepository extends EmployeeRepository
{
    public $partner;

    public function newQuery(): Builder
    {
        $query = parent::newQuery();

        if (! $this->partner) {
            throw new BadRequestException("Необходимо указать партнера"); // @todo BadRequestException
        }

        return $query
            ->whereHas('employee', function (Builder $query) {
                $query->where([
                    ['company_id', '=', $this->partner->company->id],
//                    ['user_id', '<>', $this->partner->id],// @todo Может быть добавить роль Employee к партнеру
                ]);
            });
    }

    public function setPartner(User $partner)
    {
        $this->partner = $partner;

        return $this;
    }

    public function create($input)
    {
        $user = parent::create($input);

        $user->companies()->attach($this->partner->company->id);

        $user->syncRoles(Role::EMPLOYEE);

        $user->syncPermissions($input['permissions']); // @TODO Permissions for Employee

        return $this->modelWrapper($user);
    }

    public function update($input, $id)
    {
        $user = parent::update($input, $id);

        if (isset($input['permissions'])) {
            $user->syncPermissions($input['permissions']); // @TODO Permissions for Employee
        }


        return $this->modelWrapper($user);
    }

    public function delete($id)
    {
        $query = $this->newQuery();

        $model = $query->findOrFail($id);

        $model->phones()->delete();

        $model->employee()->delete();

        $model->delete();

        return true;
    }

    public function resource()
    {
        return PartnerEmployeeResource::class;
    }

    public function resourceCollection()
    {
        return PartnerEmployeeCollection::class;
    }
}