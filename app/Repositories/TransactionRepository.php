<?php

namespace App\Repositories;

use App\Models\Client;
use App\Models\Journal;
use App\Models\Transaction;
use App\Models\TransactionOption;
use App\Models\TransactionOptionType;
use App\Repositories\BaseRepository;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

/**
 * Class TransactionRepository
 * @package App\Repositories
 * @version June 16, 2020, 7:52 am UTC
*/

class TransactionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $allowedFilters = [
        'handleable_type',
        'handleable_id',
        'client_id',
        'type'
    ];

    /**
     * @var array
     */
    protected $allowedFields = [
        'handleable_type',
        'handleable_id',
        'client_id',
        'type'
    ];

    /**
     * @var array
     */
    protected $allowedSorts = [
        'handleable_type',
        'handleable_id',
        'client_id',
        'type'
    ];

    /**
     * @var array
     */
    protected $allowedIncludes = [];

    /**
     * @var array
     */
    protected $allowedAppends = [];

    /**
     * Return allowed filters
     *
     * @return array
     */
    public function getAllowedFilters()
    {
        return $this->allowedFilters;
    }

    /**
     * Return allowed fields
     *
     * @return array
     */
    public function getAllowedFields()
    {
        return $this->allowedFields;
    }

    /**
     * Return allowed sorts
     *
     * @return array
     */
    public function getAllowedSorts()
    {
        return $this->allowedSorts;
    }

    /**
     * Return allowed includes
     *
     * @return array
     */
    public function getAllowedIncludes()
    {
        return $this->allowedIncludes;
    }

    /**
     * Return allowed appends
     *
     * @return array
     */
    public function getAllowedAppends()
    {
        return $this->allowedAppends;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Transaction::class;
    }

    public function create($input)
    {
        $modelInput = collect($input)->only([
            'handleable_type',
            'handleable_id',
            'client_id',
            'type',
        ])->toArray();

        $client = Client::findOrFail($modelInput['client_id']);

        switch ($modelInput['handleable_type']) {
            case 'users':
                $modelInput['handleable_type'] = 'App\Models\User';
                break;
            default:
                throw new BadRequestException('Неизвестный handleable тип'); // @todo BadRequestException
        }

        $model = $this->model->newInstance($modelInput);

        $model->save();

        collect($input['options'])
            ->only([TransactionOptionType::POINTS_SPENT, TransactionOptionType::POINTS_RECEIVED])
            ->each(function($option, $optionName) use($client, $modelInput) {
                if ($optionName === TransactionOptionType::POINTS_SPENT) {
                    $client->balance -= $option;
                    $client->save();
                    Journal::create([
                        'user_id' => $modelInput['handleable_id'],
                        'company_id' => $client->wallet->company_id,
                        'wallet_id' => $client->wallet_id,
                        'message' => 'Списание баллов: ' . $option . ' у клиента: ' . $client->serial_number . "({$client->name})",
                        'type' => 'points',
                    ]);
                }

                if ($optionName === TransactionOptionType::POINTS_RECEIVED) {
                    $client->balance += $option;
                    $client->save();
                    Journal::create([
                        'user_id' => $modelInput['handleable_id'],
                        'company_id' => $client->wallet->company_id,
                        'wallet_id' => $client->wallet_id,
                        'message' => 'Начисление баллов: ' . $option . ' у клиента: ' . $client->serial_number . "({$client->name})",
                        'type' => 'points'
                    ]);
                }
            });

        $options = collect($input['options'])
            ->only(TransactionOptionType::defaultTypes())
            ->map(function($option, $optionName) {
                return TransactionOption::instanceFromOption($option, $optionName);
            })
            ->flatten()
            ->all();

        $model->options()->saveMany($options);

        return $model;
    }
}
