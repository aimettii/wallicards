<?php

namespace App\Repositories;

use App\Models\WalletCertificate;
use App\Repositories\BaseRepository;

/**
 * Class WalletCertificateRepository
 * @package App\Repositories
 * @version June 15, 2020, 9:16 am UTC
*/

class WalletCertificateRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $allowedFilters = [
        
    ];

    /**
     * @var array
     */
    protected $allowedFields = [
        
    ];

    /**
     * @var array
     */
    protected $allowedSorts = [
        
    ];

    /**
     * @var array
     */
    protected $allowedIncludes = [];

    /**
     * @var array
     */
    protected $allowedAppends = [];

    /**
     * Return allowed filters
     *
     * @return array
     */
    public function getAllowedFilters()
    {
        return $this->allowedFilters;
    }

    /**
     * Return allowed fields
     *
     * @return array
     */
    public function getAllowedFields()
    {
        return $this->allowedFields;
    }

    /**
     * Return allowed sorts
     *
     * @return array
     */
    public function getAllowedSorts()
    {
        return $this->allowedSorts;
    }

    /**
     * Return allowed includes
     *
     * @return array
     */
    public function getAllowedIncludes()
    {
        return $this->allowedIncludes;
    }

    /**
     * Return allowed appends
     *
     * @return array
     */
    public function getAllowedAppends()
    {
        return $this->allowedAppends;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return WalletCertificate::class;
    }
}
