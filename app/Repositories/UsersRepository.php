<?php

namespace App\Repositories;

use App\Models\User;
use App\Repositories\BaseRepository;

/**
 * Class ArticleRepository
 * @package App\Repositories
 * @version December 27, 2019, 1:53 pm UTC
 */

class UsersRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $allowedFilters = [
        'name'
    ];

    /**
     * @var array
     */
    protected $allowedFields = [
        'name'
    ];

    /**
     * @var array
     */
    protected $allowedSorts = [
        'name'
    ];

    /**
     * @var array
     */
    protected $allowedIncludes = [];
    /**
     * @var array
     */
    protected $allowedAppends = [];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getAllowedFilters()
    {
        return $this->allowedFilters;
    }

    public function getAllowedFields()
    {
        return $this->allowedFields;
    }

    public function getAllowedSorts()
    {
        return $this->allowedSorts;
    }

    public function getAllowedIncludes()
    {
        return $this->allowedIncludes;
    }

    public function getAllowedAppends()
    {
        return $this->allowedAppends;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return User::class;
    }

    /**
     * @inherit
     */
    public function create($input)
    {
        $input['password'] = User::generatePassword($input['password']);

        $model = $this->model->newInstance($input);

        $model->save();

        $model->createPhones($input['phone']);

        return $this->modelWrapper($model);
    }


    /**
     * @inherit
     */
    public function update($input, $id)
    {
        $query = $this->model->newQuery();

        $model = $query->findOrFail($id);

        if (isset($input['password'])) {
            $input['password'] = User::generatePassword($input['password']);
        }

        $model->fill($input);

        $model->save();

        if (isset($input['phone'])) {
            $model->createPhones($input['phone']);
        }

        return $this->modelWrapper($model);
    }
}
