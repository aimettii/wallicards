<?php


namespace App\Repositories\Company;

use App\Models\Company;
use App\Repositories\WalletCertificateRepository;
use Illuminate\Database\Eloquent\Builder;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class CompanyWalletCertificatesRepository extends  WalletCertificateRepository
{
    use \CompanySetterTrait;

    public function newQuery(): Builder
    {
        $query = parent::newQuery();

        if (! $this->company) {
            throw new BadRequestException("Необходимо указать компанию"); // @todo BadRequestException
        }

        return $query->where([
            ['company_id', '=', $this->company->id]
        ]);
    }

}