<?php


namespace App\Repositories\Company;

use App\Http\Resources\CompanyPushesCollection;
use App\Http\Resources\CompanyPushesResource;
use App\Repositories\PushRepository;
use Illuminate\Database\Eloquent\Builder;

class CompanyPushRepository extends PushRepository
{
    use \CompanySetterTrait;

    /**
     * @return Builder
     * @throws \Exception
     */
    public function newQuery() : Builder
    {
        $query = parent::newQuery();

        if (!$this->company) {
            throw new \Exception('Необходимо указать компанию');
        }

        $query->whereHas('company', function($query) {
            return $query->where('companies.id', $this->company->id);
        });

        return $query;
    }

    public function create($input)
    {
        $model = parent::create($input);

        return $model;
    }

    public function resource()
    {
        return CompanyPushesResource::class;
    }

    public function resourceCollection()
    {
        return CompanyPushesCollection::class;
    }
}