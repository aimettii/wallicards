<?php

use App\Models\Company;

trait CompanySetterTrait {
    public $company;

    public function setCompany(Company $company)
    {
        $this->company = $company;

        return $this;
    }
}
