<?php


namespace App\Repositories\Company;

use App\Http\Resources\CompanyClientTransactionsCollection;
use App\Http\Resources\CompanyClientTransactionsResource;
use App\Repositories\TransactionRepository;
use Illuminate\Database\Eloquent\Builder;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class CompanyClientTransactionsRepository extends TransactionRepository
{
    use \ClientSetterTrait;

    /**
     * @return Builder
     * @throws \Exception
     */
    public function newQuery(): Builder
    {
        $query = parent::newQuery();

        if (! $this->client) {
            throw new BadRequestException("Необходимо указать клиента"); // @todo BadRequestException
        }

        $query->where('client_id', $this->client->id);

        return $query;
    }

    public function resource()
    {
        return CompanyClientTransactionsResource::class;
    }

    public function resourceCollection()
    {
        return CompanyClientTransactionsCollection::class;
    }
}