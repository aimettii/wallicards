<?php

use App\Models\Client;

trait ClientSetterTrait {
    public $client;

    public function setClient(Client $client)
    {
        $this->client = $client;

        return $this;
    }
}
