<?php


namespace App\Repositories\Company;

use App\Helpers\CollectionHelper;
use App\Http\Resources\CompanyClientPointsHistoryCollection;
use App\Http\Resources\CompanyClientPointsHistoryResource;
use App\Models\TransactionOptionType;
use App\Repositories\TransactionRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class CompanyClientPointsHistoryRepository extends TransactionRepository
{
    use \ClientSetterTrait;

    /**
     * @return Builder
     * @throws \Exception
     */
    public function newQuery(): Builder
    {
        $query = parent::newQuery();

        if (!$this->client) {
            throw new BadRequestException("Необходимо указать клиента"); // @todo BadRequestException
        }

        $query->where('client_id', $this->client->id);

        $query->with([
            'options' => function($query) {
                $query->whereHas('type', function($query) {
                    $query->whereIn('name', [TransactionOptionType::POINTS_SPENT, TransactionOptionType::POINTS_RECEIVED]);
                });
            }
        ]);

        return $query;
    }

    public function paginate()
    {
        return 'paginate';

//        return $query->paginate($this->listParameters->pagination('per_page'))->appends(request()->query());
    }

    public function all($query = null) {
        $pagination =  $query === 'paginate';
        $query = $this->allQuery();

        $resource = $this->resource();
        $resourceCollection = $this->resourceCollection();

        $items = $query->get();

        $pointsHistory = collect([]);

        foreach ($items as $transaction) {
            $transaction->load('options');

            $options = $transaction->options;

            $note = $options->filter(function($option) {
                return $option->typeName === TransactionOptionType::NOTE;
            })->first();

            foreach ($options as $option) {
                if (
                collect([TransactionOptionType::POINTS_SPENT, TransactionOptionType::POINTS_RECEIVED])
                    ->contains($option->typeName)
                ) {
                    $pointsHistory->push([
                        'transaction' => $transaction,
                        'note' => $note ? $note['value'] : null,
                        'type' => $option['type_name'],
                        'points' => $option['value'],
                        'created_at' => $transaction->created_at,
                        'updated_at' => $transaction->updated_at,
                    ]);
                }
            }
        }

        $items = $pagination ? CollectionHelper::paginate($pointsHistory, $this->listParameters->pagination('per_page') ? $this->listParameters->pagination('per_page') : 15) : $pointsHistory;

        if ($items instanceof LengthAwarePaginator && $resourceCollection) {
            return new $resourceCollection($items);
        }

        if (($items instanceof Collection || $items instanceof \Illuminate\Support\Collection) && $resource) {
            return $resource::collection($items);
        }

        return $items;
    }

    public function resource()
    {
        return CompanyClientPointsHistoryResource::class;
    }

    public function resourceCollection()
    {
        return CompanyClientPointsHistoryCollection::class;
    }
}