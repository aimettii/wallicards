<?php


namespace App\Repositories\Company;

use App\Http\Resources\CompanyClientsCollection;
use App\Http\Resources\CompanyClientsResource;
use App\Models\Client;
use App\Models\Wallet;
use Illuminate\Database\Eloquent\Builder;
use App\Repositories\ClientRepository;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class CompanyClientsRepository extends ClientRepository
{
    use \CompanySetterTrait;

    public $walletIds;

    /**
     * @return Builder
     * @throws \Exception
     */
    public function newQuery(): Builder
    {
        $query = parent::newQuery();

        if (! $this->company) {
            throw new BadRequestException("Необходимо указать компанию"); // @todo BadRequestException
        }


//        $walletsRepository = new CompanyWalletsRepository(app());
//
//        $walletsRepository->setCompany($this->company);

        $this->walletIds = Wallet::where('company_id', $this->company->id)->get()->pluck('id');

        return $query->whereIn('wallet_id', $this->walletIds);
    }

    public function create($input)
    {
        $data = collect($input)->only([
            'name',
            'gender',
            'date_of_birth',
            'balance',
            'wallet_id',
        ])->all();

        $validWallet = Wallet::where('company_id', $this->company->id)->where('id', $data['wallet_id'])->first(); // @todo Сделать более удобную проверку на допустимость валлета

        if (!$validWallet) {
            throw new BadRequestException("Недействительный шаблон карты"); // @todo BadRequestException
        }

        $wallet = Wallet::findOrFail($data['wallet_id']);

        $data['serial_number'] = Client::generateSerialNumber();

        if (!array_key_exists('balance', $data) || ! $data['balance']) {
            $data['balance'] = Client::generateWelcomeBalance($wallet);
        }

        $data['status_on_device'] = Client::STATUS_DEVICE_NOT_ACTIVE;

        $model = $this->model->newInstance($data);

        $model->save();

        $model->createPhones($input['phone']);

        return $model;
    }

    public function update($input, $id)
    {
        $data = collect($input)->only([
            'name',
            'gender',
            'date_of_birth',
            'balance',
            'favorite'
        ])->all();

        $model = parent::update($data, $id);

        return $model;
    }

    public function resource()
    {
        return CompanyClientsResource::class;
    }

    public function resourceCollection()
    {
        return CompanyClientsCollection::class;
    }
}