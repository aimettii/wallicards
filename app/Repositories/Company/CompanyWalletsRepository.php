<?php

namespace App\Repositories\Company;

use App\Http\Resources\CompanyWalletsResource;
use App\Models\Company;
use App\Models\WalletPropertyType;
use App\Models\WalletPropertyValues;
use App\Repositories\WalletRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

/**
 * Class CompanyWalletsRepository
 * @package App\Repositories
 * @version June 10, 2020, 11:42 am UTC
*/

class CompanyWalletsRepository extends WalletRepository
{
    use \CompanySetterTrait;

    public function newQuery(): Builder
    {
        $query = parent::newQuery();

        if (! $this->company) {
            throw new BadRequestException("Необходимо указать компанию"); // @todo BadRequestException
        }

        return $query->where([
            ['company_id', '=', $this->company->id]
        ]);
    }

    public function create($input)
    {
        $modelInput = collect($input)->only([
            'wallet_type_id',
            'name',
            'certificate_id'
        ])->all();

        $modelInput['uuid'] = Str::uuid();
        $modelInput['company_id'] = $this->company->id; // Текущая компания соотрудника или партнера, создающего wallet карту

        $model = $this->model->newInstance($modelInput);

        $model->save();

        $options = collect($input['options'])
            ->only(WalletPropertyType::getAllTypes())
            ->map(function($option, $optionName) use ($model) {
                return WalletPropertyValues::instanceFromOption($option, $optionName, $model);
            })
            ->flatten(1)
            ->all();

        $model->options()->saveMany($options);

        $resource = $this->resource();

        return $resource ? new $resource($model) : $model;
    }

    public function update($input, $id)
    {
        $query = $this->newQuery();

        $modelInput = collect($input)->only([
            'wallet_type_id',
            'name',
            'certificate_id'
        ])->all();

        $model = $query->findOrFail($id);

        $model->fill($modelInput);

        $model->save();

        $model->options()->delete();

        $options = collect($input['options'])
            ->only(WalletPropertyType::getAllTypes())
            ->map(function($option, $optionName) use ($model) {
                return WalletPropertyValues::instanceFromOption($option, $optionName, $model);
            })
            ->flatten(1)
            ->all();

        $model->options()->saveMany($options);

        $resource = $this->resource();

        return $resource ? new $resource($model) : $model;
    }

    public function resource()
    {
        return CompanyWalletsResource::class;
    }
}
