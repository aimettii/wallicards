<?php

namespace App\Repositories;

use App\Models\Wallet;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class WalletRepository
 * @package App\Repositories
 * @version June 16, 2020, 8:52 am UTC
*/

class WalletRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $allowedFilters = [
        'name',
        'company_id',
        'wallet_type_id'
    ];

    /**
     * @var array
     */
    protected $allowedFields = [
        'name',
        'company_id',
        'wallet_type_id'
    ];

    /**
     * @var array
     */
    protected $allowedSorts = [
        'name',
        'company_id',
        'wallet_type_id'
    ];

    /**
     * @var array
     */
    protected $allowedIncludes = [];

    /**
     * @var array
     */
    protected $allowedAppends = [];

    /**
     * Return allowed filters
     *
     * @return array
     */
    public function getAllowedFilters()
    {
        return $this->allowedFilters;
    }

    /**
     * Return allowed fields
     *
     * @return array
     */
    public function getAllowedFields()
    {
        return $this->allowedFields;
    }

    /**
     * Return allowed sorts
     *
     * @return array
     */
    public function getAllowedSorts()
    {
        return $this->allowedSorts;
    }

    /**
     * Return allowed includes
     *
     * @return array
     */
    public function getAllowedIncludes()
    {
        return $this->allowedIncludes;
    }

    /**
     * Return allowed appends
     *
     * @return array
     */
    public function getAllowedAppends()
    {
        return $this->allowedAppends;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Wallet::class;
    }

    /**
     * @param string $uuid
     * @return Wallet|null
     */
    public function findByUuid(string $uuid)
    {
        $query = $this->newQuery();

        return $query->where('uuid', $uuid)->first();
    }

    /**
     * {@inherit}
     */
    public function delete($id)
    {
        $query = $this->newQuery();

        $model = $query->findOrFail($id);

        return $model->delete();
    }
}
