<?php

namespace App\Repositories;

use App\Models\Role;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class EmployeeRepository
 * @package App\Repositories
 * @version June 10, 2020, 11:48 am UTC
*/

class EmployeeRepository extends UsersRepository
{
    public function newQuery(): Builder
    {
        $query = parent::newQuery();

        return $query->role([Role::EMPLOYEE, Role::PARTNER]); // @todo Может быть добавить роль Employee к партнеру
    }
}
