<?php

namespace App\Repositories;

use App\Models\Company;
use App\Models\Employee;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
/**
 * Class PartnerRepository
 * @package App\Repositories
 * @version June 10, 2020, 3:48 pm UTC
*/

class PartnerRepository extends UsersRepository
{
    public function newQuery() : Builder
    {
        $query = parent::newQuery();

        return $query->role(Role::PARTNER);
    }

    public function create($input)
    {
        $partner = parent::create($input);

        return $partner;
    }

    public function delete($id)
    {
        $query = $this->newQuery();

        $model = $query->findOrFail($id);

        $model->phones()->delete();

        if ($model->company) {
            $employees = Employee::where([
                'company_id' => $model->company->id
            ]);

            $usersIds = $employees->get()->pluck(['user_id']);
            $companyId = $model->company->id;

            $employees->delete();

            User::destroy($usersIds); // @todo Create users delete functionally in Model i think
            Company::destroy($companyId); // @todo Create company delete functionally in Model i think
        }

        return true;
    }
}
