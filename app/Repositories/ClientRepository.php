<?php

namespace App\Repositories;

use App\Models\Client;
use App\Models\Wallet;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Builder;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

/**
 * Class ClientRepository
 * @package App\Repositories
 * @version June 16, 2020, 7:40 am UTC
*/

class ClientRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $allowedFilters = [
        'serial_number',
        'wallet_id',
        'name',
        'gender',
        'date_of_birth',
        'balance',
        'device_id',
        'push_token',
        'platform',
        'status_on_device',
        'created_at',
        'updated_at'
    ];

    /**
     * @var array
     */
    protected $allowedFields = [
        'serial_number',
        'wallet_id',
        'name',
        'gender',
        'date_of_birth',
        'balance',
        'device_id',
        'push_token',
        'platform',
        'status_on_device'
    ];

    /**
     * @var array
     */
    protected $allowedSorts = [
        'serial_number',
        'wallet_id',
        'name',
        'gender',
        'date_of_birth',
        'balance',
        'device_id',
        'push_token',
        'platform',
        'status_on_device'
    ];

    /**
     * @var array
     */
    protected $allowedIncludes = [];

    /**
     * @var array
     */
    protected $allowedAppends = [];

    /**
     * Return allowed filters
     *
     * @return array
     */
    public function getAllowedFilters()
    {
        return $this->allowedFilters;
    }

    /**
     * Return allowed fields
     *
     * @return array
     */
    public function getAllowedFields()
    {
        return $this->allowedFields;
    }

    /**
     * Return allowed sorts
     *
     * @return array
     */
    public function getAllowedSorts()
    {
        return $this->allowedSorts;
    }

    /**
     * Return allowed includes
     *
     * @return array
     */
    public function getAllowedIncludes()
    {
        return $this->allowedIncludes;
    }

    /**
     * Return allowed appends
     *
     * @return array
     */
    public function getAllowedAppends()
    {
        return $this->allowedAppends;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Client::class;
    }

    public $wallet;

    public function setWallet(Wallet $wallet)
    {
        $this->wallet = $wallet;

        return $this;
    }

    public function newQuery() : Builder
    {
        $query = parent::newQuery();

        if ($this->wallet) {
            $query->where(['wallet_id' => $this->wallet->id]);
        }

        return $query;
    }

    public function create($input)
    {
        if (! $this->wallet) {
            throw new BadRequestException("Необходимо указать шаблон wallet"); // @todo BadRequestException
        }

        $data = collect($input)->only([
            'name',
            'gender',
            'date_of_birth',
            'platform'
        ])->all();

        $data['wallet_id'] = $this->wallet->id;

        $data['serial_number'] = Client::generateSerialNumber();
        $data['balance'] = Client::generateWelcomeBalance($this->wallet);

        $data['status_on_device'] = Client::STATUS_DEVICE_NOT_ACTIVE;

        $model = $this->model->newInstance($data);

        $model->save();

        $model->createPhones($input['phone']);

        return $model;
    }

    public function findBySerial($serial)
    {
        $query = $this->newQuery();

        return $query->where('serial_number', $serial)->first();
    }
}
