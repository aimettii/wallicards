<?php

namespace App\Providers;

use App\Services\SMS;
use App\Services\WalletOptions;
use App\Services\WalletPass;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
//        $this->app->bind(
//            'App\Contracts\WalletPass',
//            'App\Services\WalletPass'
//        );

        $this->app->bind('wallet_pass', function () {
            return new WalletPass();
        });

        $this->app->bind('wallet_options', function () {
            return new WalletOptions();
        });

        $this->app->bind('sms', function () {
            return new SMS();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
//        JsonResource::withoutWrapping();
    }
}
