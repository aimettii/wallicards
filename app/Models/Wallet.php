<?php

namespace App\Models;

use App\Models\WalletType;
use Illuminate\Database\Eloquent\Model;

/**
 * @SWG\Definition(
 *      definition="Wallet",
 *      required={"name", "company_id", "wallet_type_id"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="company_id",
 *          description="company_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="wallet_type_id",
 *          description="wallet_type_id",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Wallet extends Model
{
    use \Staudenmeir\EloquentHasManyDeep\HasTableAlias;

    public $table = 'wallets';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    const TYPE_DISCOUNT = 'discount';
    const TYPE_BONUS = 'bonus';
    const TYPE_COUPON = 'coupon';
    const TYPE_CHOP = 'chop';

    protected $fillable = [
        'uuid',
        'name',
        'company_id',
        'wallet_type_id',
        'certificate_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'uuid' => 'string',
        'name' => 'string',
        'company_id' => 'integer',
        'wallet_type_id' => 'integer',
        'certificate_id' => 'integer',
    ];

//    public $appends = ['type_name'];

    public function getTypeNameAttribute()
    {
        return $this->type ? $this->type->name : null;
    }

    public function getBackgroundImageUrlAttribute()
    {
        $walletOptions = \WalletOptions::setWalletInstance($this);

        return $walletOptions->imageOption(WalletPropertyType::BACKGROUND_IMAGE)
            ? url('storage/wallet/' . $this->uuid . '/background-image')
            : null;
    }

    public function getLogoImageUrlAttribute()
    {
        $walletOptions = \WalletOptions::setWalletInstance($this);

        return $walletOptions->imageOption(WalletPropertyType::LOGO_IMAGE)
            ? url('storage/wallet/' . $this->uuid . '/logo-image')
            : null;
    }

    public function getPushNotificationIconUrlAttribute()
    {
        $walletOptions = \WalletOptions::setWalletInstance($this);

        return $walletOptions->imageOption(WalletPropertyType::PUSH_NOTIFICATION_ICON)
            ? url('storage/wallet/' . $this->uuid . '/push-notification-icon-image')
            : null;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function company()
    {
        return $this->belongsTo(\App\Models\Company::class, 'company_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function type()
    {
        return $this->belongsTo(WalletType::class, 'wallet_type_id');
    }

    public function certificate()
    {
        return $this->belongsTo(\App\Models\WalletCertificate::class, 'company_id');
    }

    public function options()
    {
        return $this->hasMany(WalletPropertyValues::class, 'wallet_id');
    }
}