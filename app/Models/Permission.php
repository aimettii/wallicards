<?php

namespace App\Models;

class Permission extends \Spatie\Permission\Models\Permission {
    public static function defaultPermissions()
    {
        return [
            'view_users',
            'add_users',
            'edit_users',
            'delete_users',

            'view_roles',
            'add_roles',
            'edit_roles',
            'delete_roles',
        ];
    }

    public static function employeePermissions()
    {
        return [
            'view_clients',
            'add_clients',
            'edit_clients',
            'delete_clients',
            'view_employees',
            'add_employees',
            'edit_employees',
            'delete_employees',
            'view_pushes',
            'add_pushes',
            'view_wallets',
            'add_wallets',
            'edit_wallets',
            'delete_wallets',
        ];
    }
}