<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

/**
 * @SWG\Definition(
 *      definition="TransactionOptionType",
 *      required={"name"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      )
 * )
 */
class TransactionOptionType extends Model
{
    public $table = 'transaction_property_types';
    
    const AMOUNT_PURCHASE = 'amount_purchase'; // Суммка покупки
    const POINTS_SPENT = 'points_spent'; // Баллов потрачено
    const POINTS_RECEIVED = 'points_received'; // Баллов получено
    const NOTE = 'note'; // Заметка кассира
    const CASHBACK_PERCENTS = 'cashback_percents'; // Процент кэшбека
    const DISCOUNT_PERCENTS = 'discount_percents'; // Процент скидки

    public static function defaultTypes()
    {
        return [
            self::AMOUNT_PURCHASE, self::POINTS_SPENT, self::POINTS_RECEIVED, self::NOTE,
            self::CASHBACK_PERCENTS, self::DISCOUNT_PERCENTS
        ];
    }

    public static function schemeOfTypes()
    {
        return [
            'percents' => [
                self::CASHBACK_PERCENTS,
                self::DISCOUNT_PERCENTS,
            ],
            'string' => [
                self::NOTE,
            ],
            'integer' => [
                self::POINTS_RECEIVED,
                self::POINTS_SPENT,
            ],
            'float' => [
                self::AMOUNT_PURCHASE,
            ]
        ];
    }

    public static function getTypeOfOption($option) {
        foreach (self::schemeOfTypes() as $keyType => $options) {
            if (in_array($option, $options)) {
                return $keyType;
            }
        }

        throw new BadRequestException('Данной опции ' . $option . ' не существует в ' . self::class); //@todo BadRequestException
    }
}
