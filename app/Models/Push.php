<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @SWG\Definition(
 *      definition="Push",
 *      required={"handleable_type", "handleable_id", "message", "will_be_executed_at"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="handleable_type",
 *          description="handleable_type",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="handleable_id",
 *          description="handleable_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="message",
 *          description="message",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="will_be_executed_at",
 *          description="will_be_executed_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Push extends Model
{
    use \Staudenmeir\EloquentHasManyDeep\HasRelationships, \Staudenmeir\EloquentHasManyDeep\HasTableAlias;

    public $table = 'pushes';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    const STATUS_PERFORMED = 'performed';
    const STATUS_FAILED = 'failed';
    const STATUS_PROCESS = 'process';

    public $fillable = [
        'name',
        'handleable_type',
        'handleable_id',
        'message',
        'will_be_executed_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'handleable_type' => 'string',
        'handleable_id' => 'integer',
        'message' => 'string',
        'will_be_executed_at' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'handleable_type' => 'required|in:users',
        'handleable_id' => 'required|exists:users,id',
        'message' => 'required|string',
        'will_be_executed_at' => 'required|date'
    ];

    public function wasHandled()
    {
        return $this->morphTo('handleable', 'handleable_type', 'handleable_id');
    }

    public function company()
    {
        return $this->hasOneDeepFromRelations($this->wallet(), (new Wallet)->setAlias('wallets')->company());
    }

    public function wallet()
    {
        return $this->hasOneDeepFromRelations($this->clients(), (new Client)->setAlias('clients')->wallet());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function clients()
    {
        return $this->belongsToMany(\App\Models\Client::class, 'push_client', 'push_id', 'client_id')
            ->withPivot('status');
    }
}
