<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class WalletPropertyType extends Model
{
    public $table = 'wallet_property_types';

    /**
     * Бонусная карта
     */
    const CASHBACK = 'cashback';
    const WELCOME_POINTS = 'welcome_points';
        const WELCOME_POINTS_COUNT = 'welcome_points.count';
        const WELCOME_POINTS_SUM_SPENDING = 'welcome_points.sum_spending';
        const WELCOME_POINTS_IMMEDIATELY = 'welcome_points.immediately';

    const CONVERSION_POINTS = 'conversion_points';

    const BURNING_POINTS = 'burning_bonuses';
        const BURNING_POINTS_DAYS_LIMIT = 'burning_bonuses.days_limit';

    /**
     * Скидочная карта
     */
    const DISCOUNT = 'discount';

    /**
     * Купон карта
     */

    /**
     * Штамп карта (ЧОП)
     */

    /**
     * Общие поля
     */
    const GEOLOCATION = 'geolocation';
        const GEOLOCATION_MESSAGE = 'geolocation.message';
        const GEOLOCATION_LATITUDE = 'geolocation.latitude';
        const GEOLOCATION_LONGITUDE = 'geolocation.longitude';

    const PUSH_NOTIFICATION_ICON = 'push_notification_icon';
    const LOGO_IMAGE = 'logo_image';
    const BACKGROUND_IMAGE = 'background_image';
    const BACKGROUND_COLORS = 'background_colors';
    const TITLE_COLORS = 'title_colors';
    const VALUE_COLORS = 'value_colors';
    const CODE_SCAN_TYPE = 'code_scan_type';
    const NUMBER_CARD_UNDER_CODE = 'number_card_under_code';
    const USE_FEEDBACK_SYSTEM = 'use_feedback_system';

    /**
     * Различные места карт
     */
    const LOGO_TEXT = 'logo_text';
    const PRIMARY_FIELDS = 'primary_fields';
        const PRIMARY_FIELDS_LABEL = 'primary_fields.*.label';
        const PRIMARY_FIELDS_VALUE = 'primary_fields.*.value';

    const SECONDARY_FIELDS = 'secondary_fields';
        const SECONDARY_FIELDS_LABEL = 'secondary_fields.*.label';
        const SECONDARY_FIELDS_VALUE = 'secondary_fields.*.value';

    const AUXILIARY_FIELDS = 'auxiliary_fields';
        const AUXILIARY_FIELDS_LABEL = 'auxiliary_fields.*.label';
        const AUXILIARY_FIELDS_VALUE = 'auxiliary_fields.*.value';

    const HEADER_FIELDS = 'header_fields';
        const HEADER_FIELDS_LABEL = 'header_fields.*.label';
        const HEADER_FIELDS_VALUE = 'header_fields.*.value';

    const BACK_FIELDS = 'back_fields';
        const BACK_FIELDS_LABEL = 'back_fields.*.label';
        const BACK_FIELDS_VALUE = 'back_fields.*.value';

    const CHANGE_MESSAGE = 'change_message';

    public static function getAllTypes()
    {
        return [
            self::CASHBACK, self::WELCOME_POINTS, self::CONVERSION_POINTS, self::BURNING_POINTS,
            self::DISCOUNT,
            self::GEOLOCATION, self::PUSH_NOTIFICATION_ICON, self::LOGO_IMAGE, self::BACKGROUND_IMAGE,
            self::BACKGROUND_COLORS, self::TITLE_COLORS, self::VALUE_COLORS, self::CODE_SCAN_TYPE, self::NUMBER_CARD_UNDER_CODE,
            self::USE_FEEDBACK_SYSTEM,
            self::LOGO_TEXT, self::PRIMARY_FIELDS, self::SECONDARY_FIELDS, self::AUXILIARY_FIELDS, self::HEADER_FIELDS,
            self::BACK_FIELDS, self::CHANGE_MESSAGE,
        ];
    }

    public static function schemeOfTypes()
    {
        return [
            'percents' => [
                self::CASHBACK,
                self::DISCOUNT,
                self::CONVERSION_POINTS,
            ],
            'mapPoints' => [
                self::GEOLOCATION_LATITUDE,
                self::GEOLOCATION_LONGITUDE,
            ],
            'string' => [
                self::GEOLOCATION_MESSAGE,
                self::CODE_SCAN_TYPE,

                self::PRIMARY_FIELDS_LABEL,
                self::PRIMARY_FIELDS_VALUE,

                self::SECONDARY_FIELDS_LABEL,
                self::SECONDARY_FIELDS_VALUE,

                self::AUXILIARY_FIELDS_LABEL,
                self::AUXILIARY_FIELDS_VALUE,

                self::HEADER_FIELDS_LABEL,
                self::HEADER_FIELDS_VALUE,

                self::BACK_FIELDS_LABEL,
                self::BACK_FIELDS_VALUE,

                self::LOGO_TEXT,
                self::CHANGE_MESSAGE,
            ],
            'filesRelations' => [
                self::PUSH_NOTIFICATION_ICON,
                self::BACKGROUND_IMAGE,
                self::LOGO_IMAGE,
            ],
            'boolean' => [
                self::WELCOME_POINTS_IMMEDIATELY,
                self::NUMBER_CARD_UNDER_CODE,
                self::USE_FEEDBACK_SYSTEM
            ],
            'integer' => [
                self::WELCOME_POINTS_COUNT,
                self::WELCOME_POINTS_SUM_SPENDING,
                self::BURNING_POINTS_DAYS_LIMIT
            ],
            'rgb' => [
                self::BACKGROUND_COLORS,
                self::TITLE_COLORS,
                self::VALUE_COLORS,
            ],
            'nestedFields' => [
                self::GEOLOCATION,
                self::WELCOME_POINTS,
                self::BURNING_POINTS,
            ],
            'arrayFields' => [
                self::PRIMARY_FIELDS,
                self::HEADER_FIELDS,
                self::SECONDARY_FIELDS,
                self::AUXILIARY_FIELDS,
                self::BACK_FIELDS,
            ],
        ];
    }

    public static function getTypeOfOption($option) {
        foreach (self::schemeOfTypes() as $keyType => $options) {
            if (in_array($option, $options)) {
                return $keyType;
            }
        }

        throw new BadRequestException('Данной опции ' . $option . ' не существует в ' . self::class); //@todo BadRequestException
    }
}
