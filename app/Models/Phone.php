<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="Phone",
 *      required={"phone_number"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="phone_number",
 *          description="phone_number",
 *          type="string"
 *      )
 * )
 */
class Phone extends Model
{
    public $table = 'phones';

    public $fillable = [
        'phone_number',
        'phoneable_type',
        'phoneable_id',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'phone_number' => 'string',
        'phoneable_type' => 'string',
        'phoneable_id' => 'integer',
    ];

    public $timestamps = false;

    /**
     * Get the owning phoneable model.
     */
    public function phoneable()
    {
        return $this->morphTo();
    }
}
