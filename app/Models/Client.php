<?php

namespace App\Models;

use App\Facades\WalletPass;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

/**
 * @SWG\Definition(
 *      definition="Client",
 *      required={"serial_number", "wallet_id", "name", "gender", "date_of_birth", "balance", "device_id", "push_token", "platform", "status_on_device"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="serial_number",
 *          description="serial_number",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="wallet_id",
 *          description="wallet_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="gender",
 *          description="gender",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="date_of_birth",
 *          description="date_of_birth",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="balance",
 *          description="balance",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="device_id",
 *          description="device_id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="push_token",
 *          description="push_token",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="platform",
 *          description="platform",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="status_on_device",
 *          description="status_on_device",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Client extends Model
{
    use \Staudenmeir\EloquentHasManyDeep\HasTableAlias;

    public $table = 'clients';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    const GENDER_MAN = 'man';
    const GENDER_WOMAN = 'woman';

    const PLATFORM_IOS = 'ios';
    const PLATFORM_ANDROID = 'android';

    const STATUS_DEVICE_ACTIVE = 'active';
    const STATUS_DEVICE_NOT_ACTIVE = 'not_active';
    const STATUS_DEVICE_DELETED = 'deleted';

    protected $dates = ['date_of_birth'];

    public $fillable = [
        'serial_number',
        'wallet_id',
        'name',
        'gender',
        'date_of_birth',
        'balance',
        'device_id',
        'push_token',
        'platform',
        'status_on_device',
        'pkpass_file',
        'favorite'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'serial_number' => 'integer',
        'wallet_id' => 'integer',
        'name' => 'string',
        'gender' => 'string',
        'date_of_birth' => 'date',
        'balance' => 'integer',
        'device_id' => 'string',
        'push_token' => 'string',
        'platform' => 'string',
        'status_on_device' => 'string',
        'pkpass_file' => 'string',
        'favorite' => 'boolean',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'wallet_id' => 'required',
        'name' => 'required',
        'gender' => 'required',
        'date_of_birth' => 'required',
        'balance' => 'required',
        'device_id' => 'required',
        'push_token' => 'required',
        'platform' => 'required',
        'status_on_device' => 'required'
    ];

    public $appends = ['passFile', 'phone'];

    public static function defaultGenders()
    {
        return [self::GENDER_MAN, self::GENDER_WOMAN];
    }

    public static function defaultMobilePlatforms()
    {
        return [self::PLATFORM_ANDROID, self::PLATFORM_IOS];
    }

    public static function defaultDeviceStatuses()
    {
        return [self::STATUS_DEVICE_ACTIVE, self::STATUS_DEVICE_NOT_ACTIVE, self::STATUS_DEVICE_DELETED];
    }

    public static function generateSerialNumber()
    {
        $maxSerial = self::max('serial_number');

        if (is_null($maxSerial)) {
            $serial = 10000000000;
        } else {
            $serial = $maxSerial + 1;
        }

        $exist = self::where('serial_number', $serial)->first();

        if ($exist) {
            throw new BadRequestException('Bad serial number was generated'); // @todo BadRequestException
        }

        return $serial;
    }

    public static function generateWelcomeBalance(Wallet $wallet)
    {
        return 0; // @todo welcome balance
    }

    public function getCashbackAttribute()
    {
        $walletOptions = \WalletOptions::setWalletInstance($this->wallet);

        return $walletOptions->value(WalletPropertyType::CASHBACK); // @todo cashback by statuses!?
    }

    public function getDiscountAttribute()
    {
        $walletOptions = \WalletOptions::setWalletInstance($this->wallet);

        return $walletOptions->value(WalletPropertyType::DISCOUNT); // @todo discount by statuses!?
    }

    public function getPhoneAttribute()
    {
        return $this->phones()->first();
    }

    public function getPassFileAttribute()
    {
        $walletPass = WalletPass::setClient($this);

        if ($walletPass->passWasExist) {
            return $walletPass->pkpassFilename();
        }

        return null;
    }

    public function getStatusAttribute()
    {
        return 'silver';
    }

    public function downloadPassFile($withName = null, $headers = [])
    {
        if (! $withName) {
            $withName = $this->pass_file;
        } else {
            $withName = $withName . '.pkpass';
        }

        return Storage::disk('client')->download($this->id . '/' . $this->pass_file, $withName, $headers);
    }

    public function updatePassFile($filename)
    {
        $this->pkpass_file = $filename;
        $this->updated_at = Carbon::now();

        $this->save();

        return $this->pkpass_file;
    }

    public function clientDirectory($filename = null)
    {
        $disk = Storage::disk('client');

        if (! $disk->exists($this->id)) {
            if (! $disk->makeDirectory($this->id)) {
                throw new BadRequestException('Не удалось создать директорию клиента'); // @todo BadRequestException
            };
        }

        return ! $filename
            ? config('filesystems.disks.client.root') . '/' . $this->id
            : $disk->get($this->id . '/' . $filename);
    }

    // @todo Перенести в трейт создание и обновление телефона
    public function createPhones($phones)
    {
        $phones = is_array($phones) ? $phones : [$phones];

        // @todo Validate phones

        // Delete all phones from phones table
        $this->phones()->delete();

        // Recreate and attach phones to user
        foreach ($phones as $phone) {
            $this->phones()->create([
                'phoneable_type' => self::class,
                'phoneable_id' => $this->id,
                'phone_number' => $phone,
            ]);
        }

        return $this->phones()->get();
    }

    public function phones()
    {
        return $this->morphMany('App\Models\Phone', 'phoneable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function wallet()
    {
        return $this->belongsTo(\App\Models\Wallet::class, 'wallet_id');
    }

    public function getCompanyAttribute()
    {
        return $this->wallet->company;
    }

    public function getPointsSpentAttribute()
    {
        $client = $this->load(['transactions.options' => function($query) {
            return $query->whereHas('type', function($query) {
                return $query->where('name', TransactionOptionType::POINTS_SPENT);
            });
        }]);

        $sum = 0;

        foreach ($client->transactions as $transaction) {
            foreach ($transaction->options as $option) {
                $sum += $option->value;
            }
        }

        return $sum;
    }

    public function getAmountPurchaseAttribute()
    {
        $client = $this->load(['transactions.options' => function($query) {
            return $query->whereHas('type', function($query) {
                return $query->where('name', TransactionOptionType::AMOUNT_PURCHASE);
            });
        }]);

        $sum = 0;

        foreach ($client->transactions as $transaction) {
            foreach ($transaction->options as $option) {
                $sum += (int) $option->value;
            }
        }

        return $sum;
    }

    public function getLastTransactionAmountPurchaseAttribute()
    {
        $transaction = $this->last_transaction;

        $sum = 0;

        if ($transaction) {
            foreach ($transaction->options as $option)
            {
                $sum += $option->value;
            }
        }

        return $sum;
    }

    public function getLastTransactionAttribute()
    {
        return $this->transactions()->with([
            'options' => function($query) {
                $query->whereHas('type', function($query) {
                    $query->where('name', TransactionOptionType::AMOUNT_PURCHASE);
                });
            }
        ])->latest()->first();
    }

    public function getLastVisitAttribute()
    {
        return $this->lastSpendingTransaction ? $this->lastSpendingTransaction->created_at : null;
    }

    public function getLastSpendingTransactionAttribute()
    {
        return $this->transactions()->where('type', Transaction::TYPE_SPENDING)->latest()->first();
    }

    public function getPointsHistoryAttribute()
    {
        $transactions = $this->transactions()->with([
            'options' => function($query) {
                $query->whereHas('type', function($query) {
                    $query->whereIn('name', [TransactionOptionType::POINTS_SPENT, TransactionOptionType::POINTS_RECEIVED]);
                });
            }
        ])->get();

        $pointsHistory = [];

        foreach ($transactions as $transaction) {
            $transaction->load('options');

            $options = $transaction->options;

            $note = $options->filter(function($option) {
                return $option->typeName === TransactionOptionType::NOTE;
            })->first();

            foreach ($options as $option) {
                if (
                    collect([TransactionOptionType::POINTS_SPENT, TransactionOptionType::POINTS_RECEIVED])
                    ->contains($option->typeName)
                ) {
                    $pointsHistory[] = [
                        'transaction' => $transaction,
                        'note' => $note ? $note['value'] : null,
                        'type' => $option['type_name'],
                        'points' => $option['value'],
                        'created_at' => $transaction->created_at,
                        'updated_at' => $transaction->updated_at,
                    ];
                }
            }
        }

        return collect($pointsHistory);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function transactions()
    {
        return $this->hasMany(\App\Models\Transaction::class, 'client_id');
    }
}
