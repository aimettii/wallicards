<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @SWG\Definition(
 *      definition="Transaction",
 *      required={"handleable_type", "handleable_id", "client_id", "type"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="handleable_type",
 *          description="handleable_type",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="handleable_id",
 *          description="handleable_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="client_id",
 *          description="client_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="type",
 *          description="type",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Transaction extends Model
{
    public $table = 'transactions';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    const TYPE_INCOME = 'income';
    const TYPE_SPENDING = 'spending';
    const TYPE_SPENDING_AND_INCOME = 'spending_and_income';

    public $fillable = [
        'handleable_type',
        'handleable_id',
        'client_id',
        'type'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'handleable_type' => 'string',
        'handleable_id' => 'integer',
        'client_id' => 'integer',
        'type' => 'string'
    ];

    /**
     * Validation rules
     * @todo Сделать нормальную валидацию в классах
     * @var array
     */
    public static $rules = [
        'handleable_type' => 'required|in:users',
        'handleable_id' => 'required',
        'client_id' => 'required',
        'type' => 'required|in:spending,income',
        'options' => 'required|array',
//        'options.' . TransactionOptionType:: => 'required|array',
    ];

    public static function defaultTypes()
    {
        return [self::TYPE_INCOME, self::TYPE_SPENDING];
    }

    public function wasHandled()
    {
        return $this->morphTo('handleable', 'handleable_type', 'handleable_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function client()
    {
        return $this->belongsTo(\App\Models\Client::class, 'client_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function options()
    {
        return $this->hasMany(\App\Models\TransactionOption::class, 'transaction_id');
    }

}
