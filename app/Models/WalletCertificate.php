<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

/**
 * @SWG\Definition(
 *      definition="WalletCertificate",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class WalletCertificate extends Model
{
    public $table = 'wallet_certificates';

    public $fillable = [
        'p12_file',
        'wwdr_file',
        'push_file',
        'p12_password',
        'team_identifier',
        'organization_name',
        'pass_type_identifier',
        'company_id',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [

    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public $appends = ['p12_path', 'wwdr_path', 'push_path'];

    public function getP12PathAttribute()
    {
        return storage_path('app/wallet_certificates/'.$this->id . '/' . $this->p12_file);
    }

    public function getWwdrPathAttribute()
    {
        return storage_path('app/wallet_certificates/'.$this->id . '/' . $this->wwdr_file);
    }

    public function getPushPathAttribute()
    {
        return storage_path('app/wallet_certificates/'.$this->id . '/' . $this->push_file);
    }
}
