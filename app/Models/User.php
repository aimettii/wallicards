<?php

namespace App\Models;

use Laravel\Sanctum\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, HasRoles;

    public $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'note'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public $appends = ['company', 'phone'];

    public function getCompanyAttribute()
    {
        $employee = $this->employee()->first();

        return $employee ? $employee->company : null;
    }

    public function getPhoneAttribute()
    {
        return $this->phones()->first();
    }

    public function company()
    {
        $employee = $this->employee()->first();

        return $employee ? $employee->company() : null; // @todo
    }

    // @todo Перенести в трейт создание и обновление телефона
    public function createPhones($phones)
    {
        $phones = is_array($phones) ? $phones : [$phones];

        // @todo Validate phones

        // Delete all phones from phones table
        $this->phones()->delete();

        // Recreate and attach phones to user
        foreach ($phones as $phone) {
            $this->phones()->create([
                'phoneable_type' => self::class,
                'phoneable_id' => $this->id,
                'phone_number' => $phone,
            ]);
        }

        return $this->phones()->get();
    }

    public function createCompany(array $input)
    {
        $company = $this->company;

        if (! $company) {
            $company = Company::create(array_merge($input, [
                'status' => Company::STATUS_ACTIVE
            ]));

            $this->companies()->attach($company->id);
        } else {
            $company->fill(
                array_merge([
                    'status' => Company::STATUS_ACTIVE
                ], $input)
            );

            $company->save();
        }

        return $company;
    }

    public function phones()
    {
        return $this->morphMany('App\Models\Phone', 'phoneable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function employee()
    {
        return $this->hasOne('App\Models\Employee', 'user_id');
    }

    public function companies()
    {
        return $this->belongsToMany('App\Models\Company', 'employees', 'user_id', 'company_id');
    }

    public static function generatePassword($password)
    {
        return bcrypt($password);
    }

    public function guardName()
    {
        return 'sanctum';
    }
}
