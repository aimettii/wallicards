<?php

namespace App\Models;

class Role extends \Spatie\Permission\Models\Role {
    const ADMIN = 'admin';
    const PARTNER = 'partner';
    const EMPLOYEE = 'employee';

    public static function defaultRoles(): array
    {
        return [self::ADMIN, self::PARTNER, self::EMPLOYEE];
    }

    public static function middlewareAnyRoles(array $roles): string
    {
        return 'role:' . implode('|', $roles);
    }

    public static function middlewareOnly(string $role): string
    {
        return 'role:' . $role;
    }
}