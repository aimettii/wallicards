<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

/**
 * @SWG\Definition(
 *      definition="TransactionOption",
 *      required={"transaction_id", "transaction_property_type_id", "transaction_property_value"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="transaction_id",
 *          description="transaction_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="transaction_property_type_id",
 *          description="transaction_property_type_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="transaction_property_value",
 *          description="transaction_property_value",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class TransactionOption extends Model
{
    public $table = 'transaction_options';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'transaction_id',
        'transaction_property_type_id',
        'transaction_property_value'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'transaction_property_value' => 'array',
        'value' => 'array'
    ];

    public $visible = ['type_name', 'value'];

    public $appends = ['type_name', 'value'];

    public static function instanceFromOption($option, $optionName)
    {
        $propertyType = TransactionOptionType::where('name', $optionName)->firstOrFail();
        $value = self::formatArrValueFromOption(...func_get_args());

        return new self([
            'transaction_property_type_id' => $propertyType->id,
            'transaction_property_value' => $value,
        ]);
    }

    public static function formatArrValueFromOption($option, $optionName)
    {
        $arrValue = [];

        $typeOfOption = TransactionOptionType::getTypeOfOption($optionName);

        switch ($typeOfOption) {
            case 'boolean':
                $arrValue['value'] = !! $option;
                break;
            case 'integer':
                $arrValue['value'] = (int) $option;
                break;
            case 'percents':
                $arrValue['value'] = (float) $option;
                break;
            case 'string':
                $arrValue['value'] = (string) $option;
                break;
            case 'float': // @todo Float типа нету в walletPropertyType (В дальнейшем нужен будет трейт)
                $arrValue['value'] = (float) $option;
                break;
            default:
                throw new BadRequestException('Неизвестный тип опции'); //@todo BadRequestException
        }

        $arrValue['type'] = $typeOfOption;

        return $arrValue;
    }

    public function getTypeNameAttribute()
    {
        return $this->type->name;
    }

    public function getValueAttribute()
    {
        return $this->transaction_property_value['value'];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function transaction()
    {
        return $this->belongsTo(\App\Models\Transaction::class, 'transaction_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function type()
    {
        return $this->belongsTo(\App\Models\TransactionOptionType::class, 'transaction_property_type_id');
    }
}
