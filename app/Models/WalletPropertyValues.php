<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class WalletPropertyValues extends Model
{
    public $table = 'wallet_property_values';

    public $fillable = [
        'wallet_id',
        'wallet_property_type_id',
        'wallet_property_value',
    ];

    public $visible = ['type', 'value'];

    public $appends = ['type', 'value'];

    public $casts = [
        'value' => 'array',
        'wallet_property_value' => 'array'
    ];

    public static function instanceFromOption($option, $optionName, Wallet $toWallet)
    {
        $propertyType = WalletPropertyType::where('name', $optionName)->firstOrFail();
        $value = self::formatArrValueFromOption(...func_get_args());

        return new self(
            [
                'wallet_property_type_id' => $propertyType->id,
                'wallet_property_value' => $value,
            ]
        );
    }

    public static function formatArrValueFromOption($option, $optionName, Wallet $toWallet)
    {
        $arrValue = [];

        $typeOfOption = WalletPropertyType::getTypeOfOption($optionName);

        switch ($typeOfOption) {
            case 'arrayFields':
                if (! is_array($option)) {
                    throw new BadRequestException('Опция типа "arrayFields" должна быть массивом'); //@todo BadRequestException
                }

                foreach ($option as $indexKey => $groupOptions) {
                    foreach ($groupOptions as $nestedOptionKey => $nestedOption) {
                        $nestedRootOptionKey = $optionName . '.*.' . $nestedOptionKey;

                        $typeOfNestedOption = WalletPropertyType::getTypeOfOption(
                            $nestedRootOptionKey
                        );

                        if ($typeOfNestedOption == 'nestedFields' || $typeOfNestedOption == 'arrayFields') {
                            throw new BadRequestException('Только 1 слой вложенности разрешен'); //@todo BadRequestException
                        }

                        $arrValue[$indexKey]['fields'][$nestedOptionKey] = $typeOfNestedOption;

                        $nestedArrValue = self::formatArrValueFromOption($nestedOption, $nestedRootOptionKey, $toWallet);

                        $arrValue[$indexKey]['value'][$nestedOptionKey] = $nestedArrValue['value'];
                    }
                }

                return $arrValue;
                break;
            case 'nestedFields':
                if (! is_array($option)) {
                    throw new BadRequestException('Опция типа "nestedFields" должна быть массивом'); //@todo BadRequestException
                }

                foreach ($option as $nestedOptionKey => $nestedOption) {
                    $nestedRootOptionKey = $optionName . '.' . $nestedOptionKey;

                    $typeOfNestedOption = WalletPropertyType::getTypeOfOption(
                        $nestedRootOptionKey
                    );

                    if ($typeOfNestedOption == 'nestedFields' || $typeOfNestedOption == 'arrayFields') {
                        throw new BadRequestException('Только 1 слой вложенности разрешен'); //@todo BadRequestException
                    }

                    $arrValue['fields'][$nestedOptionKey] = $typeOfNestedOption;

                    $nestedArrValue = self::formatArrValueFromOption($nestedOption, $nestedRootOptionKey, $toWallet);

                    $arrValue['value'][$nestedOptionKey] = $nestedArrValue['value'];
                }

                return $arrValue;
                break;
            case 'boolean':
                $arrValue['value'] = !!$option;
                break;
            case 'integer':
                $arrValue['value'] = (int) $option;
                break;
            case 'percents':
            case 'mapPoints':
                $arrValue['value'] = (float) $option;
                break;
            case 'string':
                $arrValue['value'] = (string) $option;
                break;
            case 'filesRelations':
                if (! $option instanceof \Illuminate\Http\UploadedFile) {
                    throw new BadRequestException('Опция с типом fileRelations должна быть файлом'); //@todo BadRequestException
                }

                $disk = Storage::disk('wallet_options');

                if (! $disk->exists($toWallet->id)) {
                    if (! $disk->makeDirectory($toWallet->id)) {
                        throw new BadRequestException('Не удалось создать директорию опции валлета'); // @todo BadRequestException
                    };
                }

                switch (exif_imagetype($option->getPathname())) {
                    case IMAGETYPE_JPEG:
                        $pngImage = imagepng(
                            imagecreatefromjpeg($option->getPathname()),
                            storage_path('app/wallet_options/' . $toWallet->id . '/' . $optionName . '.png')
                        );

//                        $disk->put($toWallet->id . '/' . $optionName . '.png', $pngImage);
                        break;
                    case IMAGETYPE_PNG:
                        $option->storeAs(
                            'wallet_options/' . $toWallet->id, $optionName . '.png'
                        );
                        break;
                    default:
                        throw new BadRequestException('Тип картинки должен быть либо png, либо jpg'); //@todo BadRequestException
                }

                $arrValue['value'] = $optionName . '.png'; // id of the files @todo File model create and associate this
                break;
            case 'rgb':
                $arrValue['value'] = (string) $option; // @todo Validate rgb color string
                break;
            default:
                throw new BadRequestException('Неизвестный тип опции'); //@todo BadRequestException
        }

        $arrValue['value'] = $arrValue['value'] === 'null' ? null : $arrValue['value'];
        $arrValue['type'] = $typeOfOption;

        return $arrValue;
    }

    public function getTypeAttribute()
    {
        return $this->walletPropertyType->name;
    }

    public function getValueAttribute()
    {
        return $this->wallet_property_value;
    }

    public function walletPropertyType()
    {
        return $this->belongsTo(WalletPropertyType::class);
    }
}
